import os
import sys
from os.path import abspath, dirname

from manager import Manager

from application import run_app

sys.path.insert(0, dirname(abspath(__file__)))

manager = Manager()


@manager.command
def run_local(port=9999, workers=os.cpu_count()):
    os.environ["ENVIRONMENT"] = "LOCAL"
    run_app(host="0.0.0.0", port=int(port), num_workers=int(workers), debug=False)

if __name__ == '__main__':
    manager.main()
