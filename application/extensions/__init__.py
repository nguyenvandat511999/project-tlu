from application.database import db
from gatco_acl.acl import ACL
from gatco_auth import Auth
from gatco_restapi import APIManager

from .jinja import Jinja
from .useragent import GatcoUserAgent

auth = Auth()
apimanager = APIManager()
jinja = Jinja()
racl = ACL()


def init_extensions(app):
    GatcoUserAgent.init_app(app)
    auth.init_app(app)
    apimanager.init_app(app, sqlalchemy_db=db)
    jinja.init_app(app)
    racl.init_app(app)
