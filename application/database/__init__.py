import redis
from gatco_sqlalchemy import SQLAlchemy
from pymongo import MongoClient

my_mongo = MongoClient(host="localhost", port=27017, maxPoolSize=200,
                       connect=False, serverSelectionTimeoutMS=500, connectTimeoutMS=500)
redis_db = redis.StrictRedis(host="localhost", port=6379, db=1,
                             socket_connect_timeout=2, socket_timeout=2)

mongo_db = my_mongo["python_web_core"]

db = SQLAlchemy()


def init_database(app):
    db.init_app(app)
