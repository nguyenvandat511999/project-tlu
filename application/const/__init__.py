class DocumentStatus(object):
    INITIALIZATION = 'init'  # Khoi tao
    PROCESS = 'process'  # Dang xu ly
    PENDING = 'pending'  # Cho xu ly
    RECEIVE = 'receive'  # Tiep nhan
    FINISH = 'finish'  # Hoan thanh
    WORKING = 'working'  # Dang lam


class ReportStatus(object):
    REVIEW = 'review'  # Khoi tao
    REJECT = 'reject'  # Dang xu ly
    APPROVE = 'approve'  # Cho xu ly
    FINISH = 'finish'  # Hoan thanh
