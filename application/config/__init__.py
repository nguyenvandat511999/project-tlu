class ConfigLocal(object):
    STATIC_URL = "static10"
    SQLALCHEMY_POOL_SIZE = 15
    SQLALCHEMY_MAX_OVERFLOW = 30

    SQLALCHEMY_DATABASE_URI = "postgresql://projowner:proj@2021@localhost:5432/proj"
