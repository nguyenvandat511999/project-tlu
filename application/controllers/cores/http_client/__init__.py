from sanic.response import json


def format_data_return(response_data=None, response_message=None, response_code="ERROR", response_http_code=200):
    if response_http_code == 200 or response_http_code == 204:
        return json(response_data, status=response_http_code)
    else:
        return json({
            "error": {
                "code": response_code,
                "message": response_message
            }
        }, status=response_http_code)
