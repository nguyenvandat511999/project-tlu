import hashlib


def convert_string_list_to_list(strg, separator_mark=","):
    """
    Example:
    :param strg: "A,B,C,D"
    :param separator_mark: ","
    :return: ["A", "B", "C", "D"]
    if exception appear :return None
    """
    try:
        return [sub_str.strip() for sub_str in strg.split(separator_mark)]
    except:
        pass
    return


def convert_list_to_string(lst):
    if not lst:
        return

    string_return = ""
    try:
        for data in lst:
            string_return += str(data) + ","
        return string_return
    except:
        pass
    return


def convert_string_to_long(strg):
    try:
        return int(hashlib.md5(strg.encode("utf-8")).hexdigest()[:6], 16)
    except:
        return 0
