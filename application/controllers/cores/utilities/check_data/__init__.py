from uuid import UUID
import re
from application.database import db
from application.models.staff import Staff
from application.models.account import Account
from application.models.permission import Permission
from application.models.user_role_permission import UserRolePermission


def string_is_null_or_empty(str):
    """
    Example 1:
    :param str: "" or "  " or None
    :return: True
    ---------
    Example 2:
    :param str: "  A  " or "B"
    :return: False
    """
    try:
        if str and str.strip():
            return False
    except:
        pass
    return True


def string_is_not_null_and_not_empty(str):
    """
    Example 1:
    :param str: "" or "  " or None
    :return: False
    ---------
    Example 2:
    :param str: "  A  " or "B"
    :return: True
    """
    try:
        if str and str.strip():
            return True
    except:
        pass
    return False


def list_is_null_or_empty(lst):
    """
    Example 1:
    :param lst: [1, 2, 3] or ["A", "B", "C"]
    :return: False
    ---------
    Example 2:
    :param lst: [] or None
    :return: True
    """
    try:
        if lst and len(lst) > 0:
            return False
    except:
        pass
    return True


def list_is_not_null_and_not_empty(lst):
    """
    Example 1:
    :param lst: [1, 2, 3] or ["A", "B", "C"]
    :return: True
    ---------
    Example 2:
    :param lst: [] or None
    :return: False
    """
    try:
        if lst and len(lst) > 0:
            return True
    except:
        pass
    return False


def string_is_not_valid_uuid(uid, version=4):
    """
    Example 1:
    :param uid: "483deb8f-9184-4b10-bd5e-e02df70c2905"
    :param version: 4
    :return: False
    ---------
    Example 2:
    :param uid: "A"
    :param version: 4
    :return: True
    """
    try:
        if UUID(uid).version == version:
            return False
    except:
        pass
    return True


def string_is_valid_uuid(uid, version=4):
    """
    Example 1:
    :param uid: "483deb8f-9184-4b10-bd5e-e02df70c2905"
    :param version: 4
    :return: True
    ---------
    Example 2:
    :param uid: "A"
    :param version: 4
    :return: False
    """
    try:
        if UUID(uid).version == version:
            return True
    except:
        pass
    return False


def list_string_is_valid_uuid(list_string, version=4):
    """
    Example 1:
    :param list_string: ["55f20aec-cc39-4a96-a1dd-f6684c51292a", "483deb8f-9184-4b10-bd5e-e02df70c2905"]
    :param version: 4
    :return: True
    ---------
    Example 2:
    :param list_string: ["55f20aec-cc39-4a96-a1dd-f6684c51292a", "A"]
    :param version: 4
    :return: False
    """
    try:
        for str in list_string:
            if UUID(str).version != version:
                return False
    except:
        return False
    return True


def list_string_is_not_valid_uuid(list_string, version=4):
    """
    Example 1:
    :param list_string: ["55f20aec-cc39-4a96-a1dd-f6684c51292a", "483deb8f-9184-4b10-bd5e-e02df70c2905"]
    :param version: 4
    :return: False
    ---------
    Example 2:
    :param list_string: ["55f20aec-cc39-4a96-a1dd-f6684c51292a", "A"]
    :param version: 4
    :return: True
    """
    try:
        for str in list_string:
            if UUID(str).version != version:
                return True
    except:
        return True
    return False


def is_valid_email(email):
    try:
        if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
            return True
    except:
        pass
    return False


def validate_user(token):
    user_login = token.jwt_identity
    user = db.session.query(Account).filter(
        Account.username == user_login).filter(
        Account.deleted == False).first()

    if not user or not user.staff_id:
        return 0

    staff = db.session.query(Staff).filter(
        Staff.id == user.staff_id).filter(
        Staff.deleted == False).first()

    if not staff:
        return 0
    return user


def get_user_info(token):
    user_login = token.jwt_identity
    user = db.session.query(Account).filter(
        Account.username == user_login).filter(
        Account.deleted == False).first()

    if not user or not user.staff_id:
        return 0

    staff = db.session.query(Staff).filter(
        Staff.id == user.staff_id).filter(
        Staff.deleted == False).first()

    if not staff:
        return 0
    return staff


def validate_user_login(method, token, object):
    user = validate_user(token)

    records_role_permission = db.session.query(UserRolePermission).filter(
        UserRolePermission.user_role_id == user.role_id).filter(
        UserRolePermission.deleted == False).all()

    list_id_permission = []
    for data_record_role_permission in records_role_permission:
        list_id_permission.append(str(data_record_role_permission.permission_id))

    records_permission = db.session.query(Permission).filter(
        Permission.id.in_(list_id_permission)).filter(
        Permission.deleted == False).all()

    for data_permission in records_permission:
        if data_permission.type == object and data_permission.method == method:
            return 1
    return 0
