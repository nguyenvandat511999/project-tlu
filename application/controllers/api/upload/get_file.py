import os
import aiofiles
import traceback
from sqlalchemy import *
from aiofiles import os as async_os
from sanic.response import file_stream


from application.database import db
from application.models.document import Document
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


# @jwt_required
async def get_file(request, id):
    object = 'get_file'
    method = request.method
    path_server = '/home/backend/uploads'
    path = '/home/dat/Documents/ThangLongUniversity/upload'
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            record_document = db.session.query(Document).filter(
                Document.id == id).filter(
                Document.deleted == False).first()

            if not record_document:
                return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                          response_http_code=404)

            try:
                file_path = path_server + '/' + record_document.file_name

                file_stat = await async_os.stat(file_path)
                headers = {"Content-Length": str(file_stat.st_size)}

                return await file_stream(
                    file_path,
                    headers=headers,
                    chunked=False,
                )

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
