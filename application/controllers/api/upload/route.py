from application.server import app
from application.controllers.api.upload.upload import upload
from application.controllers.api.upload.get_file import get_file

app.add_route(upload, 'upload/api/v1/web/upload-file/', methods=['POST'])
app.add_route(get_file, 'upload/api/v1/web/get-file/<id:uuid>', methods=['GET'])
