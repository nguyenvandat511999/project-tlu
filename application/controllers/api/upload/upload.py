import os
import aiofiles
import traceback
from sqlalchemy import *

from application.database import db
from application.models.document import Document
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def upload(request, token: Token):
    object = 'upload'
    method = request.method
    path_server = '/home/backend/uploads'
    path = '/home/dat/Documents/ThangLongUniversity/upload'
    try:
        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            document_file = request.files.get('file')
            # print(type(document_file))

            if not document_file:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_type_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                if not os.path.exists(path_server):
                    os.makedirs(path_server)
                async with aiofiles.open(path_server + "/" + document_file.name, 'wb') as f:
                    await f.write(document_file.body)
                f.close()

                return format_data_return(response_data={'data': {
                                                            'result': document_file.name
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
