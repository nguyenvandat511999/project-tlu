import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.department import Department
from application.models.department_document import DepartmentDocument

from application.const import DocumentStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_admin_id(request, token: Token, id):
    object = 'document_admin'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_document)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            description = data.get('description')
            department_id = data.get('department_id')

            if department_id and string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": department_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.document_id == id).filter(
                    DepartmentDocument.deleted == False).first()

                if not record_department_document:
                    return format_data_return(response_message=ErrorMessage.DATA_IS_EXISTS + ": văn bản không tồn tại",
                                              response_http_code=404)

                if department_id:
                    record_department = db.session.query(Department).filter(
                        Department.id == department_id).filter(
                        Department.deleted == False).first()

                    if not record_department:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": department không tồn tại",
                                                  response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_department_document.description = description
                record_department_document.document_id = record_document.id
                record_department_document.department_id = record_department.id if department_id is not None else record_department_document.department_id
                record_department_document.department_name = record_department.name if department_id is not None else record_department_document.department_name

                if department_id:
                    record_department.document.append(record_department_document)

                record_document.department.append(record_department_document)
                record_document.status = DocumentStatus.PROCESS

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_department_document)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


@jwt_required
async def finish_document(request, token: Token, id):
    object = 'finish_document'
    method = request.method
    try:
        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.document_id == id).filter(
                    DepartmentDocument.deleted == False).first()

                if not record_department_document:
                    return format_data_return(response_message=ErrorMessage.DATA_IS_EXISTS + ": văn bản cần xử lí không tồn tại",
                                              response_http_code=404)

                record_department_document.deleted = True
                record_department_document.status = DocumentStatus.FINISH
                record_document.status = DocumentStatus.FINISH

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_department_document)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
