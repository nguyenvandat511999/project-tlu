from application.server import app
from application.controllers.api.document_admin.document_admin import document_admin
from application.controllers.api.document_admin.document_admin_id import document_admin_id, finish_document


app.add_route(document_admin, 'document-admin/api/v1/web/document-admin/',
              methods=['POST', 'GET'])
app.add_route(document_admin_id, 'document-admin/api/v1/web/document-admin/<id:uuid>',
              methods=['PUT', 'GET'])
app.add_route(finish_document, 'document-admin/api/v1/web/finish-document/<id:uuid>',
              methods=['PUT'])
