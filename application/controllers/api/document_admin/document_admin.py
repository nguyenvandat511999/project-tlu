import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.department import Department
from application.models.department_document import DepartmentDocument

from application.const import DocumentStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_admin(request, token: Token):
    object = 'move_document'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                data = request.args
                end = data.get('end')
                start = data.get('start')
                status = data.get('status')

                record_department = db.session.query(Department).filter(
                    Department.parent_id == None).filter(
                    Department.deleted == False).first()

                # print(record_department.id)
                if not record_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": department không tồn tại",
                                              response_http_code=404)

                records_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.department_id == record_department.id).filter(
                    DepartmentDocument.deleted == False).all()
                # print(records_department_document)

                list_id_document = []
                for data_record_department_document in records_department_document:
                    list_id_document.append(str(data_record_department_document.document_id))
                # print(list_id_document)

                list_data_return = []
                num_result = 0
                for data_id in list_id_document:
                    record_document = db.session.query(Document).filter(
                        Document.id == data_id).filter(
                        Document.created_at <= start if start is not None else True).filter(
                        Document.end_at <= end if end is not None else True).filter(
                        Document.status == status if status is not None else True).filter(
                        Document.deleted == False).first()

                    record_document_return = to_dict(record_document)
                    # print(record_document_return)
                    record_department_document = db.session.query(DepartmentDocument).filter(
                        DepartmentDocument.document_id == data_id).filter(
                        DepartmentDocument.department_id != record_department.id).filter(
                        DepartmentDocument.deleted == False).first()

                    if record_department_document:
                        record_document_return['department_work'] = record_department_document.department_name
                    else:
                        record_document_return['department_work'] = None

                    list_data_return.append(record_document_return)
                    num_result += 1

                return format_data_return(response_data={'data': {
                    'result': list_data_return,
                    'total': num_result
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            description = data.get('description')
            document_id = data.get('document_id')
            department_id = data.get('department_id')

            if string_is_not_valid_uuid(document_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_id không đúng định dạng uuid",
                                          response_http_code=400)

            if string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": department_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_department = db.session.query(Department).filter(
                    Department.id == department_id).filter(
                    Department.deleted == False).first()

                if not record_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": department không tồn tại",
                                              response_http_code=404)

                record_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.document_id == document_id).filter(
                    DepartmentDocument.department_id == department_id).filter(
                    DepartmentDocument.deleted == False).first()

                if record_department_document:
                    return format_data_return(response_message=ErrorMessage.DATA_IS_EXISTS + ": văn bản đã tồn tại",
                                              response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == document_id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_department_document = DepartmentDocument()

                record_department_document.description = description
                record_department_document.document_id = record_document.id
                record_department_document.department_id = record_department.id
                record_department_document.department_name = record_department.name
                record_department_document.status = DocumentStatus.PROCESS

                record_department.document.append(record_department_document)
                record_document.department.append(record_department_document)
                record_document.status = DocumentStatus.PROCESS

                db.session.add(record_department_document)
                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_department_document)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
