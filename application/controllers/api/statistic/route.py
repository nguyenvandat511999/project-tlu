from application.server import app
from application.controllers.api.statistic.statistic_admin import statistic_admin, statistic_admin_line
from application.controllers.api.statistic.statistic_manager import statistic_manager, statistic_manager_line
from application.controllers.api.statistic.statistic_staff import statistic_staff, statistic_staff_line
from application.controllers.api.statistic.update_statistic import update_status


app.add_route(statistic_admin, 'statistic/api/v1/web/admin/',
              methods=['GET'])
app.add_route(statistic_admin_line, 'statistic-line/api/v1/web/admin/',
              methods=['GET'])

app.add_route(statistic_manager, 'statistic/api/v1/web/manager/',
              methods=['GET'])
app.add_route(statistic_manager_line, 'statistic-line/api/v1/web/manager/',
              methods=['GET'])

app.add_route(statistic_staff, 'statistic/api/v1/web/staff/',
              methods=['GET'])
app.add_route(statistic_staff_line, 'statistic-line/api/v1/web/staff/',
              methods=['GET'])

app.add_route(update_status, 'document-status/api/v1/web/update/',
              methods=['PUT'])
