import traceback
from sqlalchemy import *

from application.database import db
from application.models.document import Document
from application.models.staff_document import StaffDocument
from application.models.department_document import DepartmentDocument

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return


async def update_status(request):
    method = request.method

    try:
        if method == 'PUT':
            try:
                records_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.deleted == False).all()

                records_staff_document = db.session.query(StaffDocument).filter(
                    StaffDocument.deleted == False).all()

                for data_record in records_department_document:
                    if data_record.document_id:
                        record_document = db.session.query(Document).filter(
                            Document.id == data_record.document_id).filter(
                            Document.deleted == False).first()

                        if record_document:
                            data_record.status = record_document.status
                            data_record.document_name = record_document.name
                            data_record.document_type_id = record_document.document_type_id
                            db.session.flush()

                for data_record in records_staff_document:
                    if data_record.document_id:
                        record_document = db.session.query(Document).filter(
                            Document.id == data_record.document_id).filter(
                            Document.deleted == False).first()

                        if record_document:
                            data_record.status = record_document.status
                            data_record.document_type_id = record_document.document_type_id
                            db.session.flush()

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': [],
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
