import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.work_report import WorkReport
from application.models.document_type import DocumentType
from application.models.department_document import DepartmentDocument

from application.const import DocumentStatus, ReportStatus
from application.controllers.api.statistic import (
    data_statistic_return,
    default_timestamp_day_end,
    default_timestamp_day_start,
    format_data_return_day_manager
)
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def statistic_manager(request, token: Token):
    object = 'statistic_manager'
    method = request.method

    record_staff = get_user_info(token)

    if not record_staff:
        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                  response_http_code=404)

    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                data = request.args
                start = data.get('start')
                end = data.get('end')
                year = data.get('year')

                if year:
                    year = int(year)

                if year is None:
                    year = 2021

                if start is None and end is None:
                    start = default_timestamp_day_start(year, 1)
                    end = default_timestamp_day_end(year, 12)
                elif start and end:
                    start = int(start)
                    end = int(end)

                sum_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.department_id == record_staff.department_id).filter(
                    DepartmentDocument.created_at >= start if start is not None else True).filter(
                    DepartmentDocument.created_at <= end if end is not None else True).filter(
                    DepartmentDocument.deleted == False).count()

                sum_document_finish = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.department_id == record_staff.department_id).filter(
                    DepartmentDocument.created_at >= start if start is not None else True).filter(
                    DepartmentDocument.created_at <= end if end is not None else True).filter(
                    DepartmentDocument.status == DocumentStatus.FINISH).filter(
                    DepartmentDocument.deleted == False).count()

                sum_document_not_finish = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.department_id == record_staff.department_id).filter(
                    DepartmentDocument.created_at >= start if start is not None else True).filter(
                    DepartmentDocument.created_at <= end if end is not None else True).filter(
                    DepartmentDocument.status != DocumentStatus.FINISH).filter(
                    DepartmentDocument.deleted == False).count()

                records_document_type = db.session.query(DocumentType).filter(
                    DocumentType.deleted == False).all()

                list_record_document_type = []
                for data_record_document_type in records_document_type:
                    data = {}
                    data['name'] = data_record_document_type.name
                    data['value'] = db.session.query(DepartmentDocument).filter(
                        DepartmentDocument.document_type_id == data_record_document_type.id).filter(
                        DepartmentDocument.created_at >= start if start is not None else True).filter(
                        DepartmentDocument.created_at <= end if end is not None else True).filter(
                        DepartmentDocument.deleted == False).count()

                    list_record_document_type.append(data)

                data_return = data_statistic_return(sum_document, sum_document_finish, sum_document_not_finish, list_record_document_type)

                return format_data_return(response_data={'data': {
                                                            'result': data_return,
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


@jwt_required
async def statistic_manager_line(request, token: Token):
    object = 'statistic_manager_line'
    method = request.method

    record_staff = get_user_info(token)

    if not record_staff:
        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                  response_http_code=404)

    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                data = request.args
                start = data.get('start')
                end = data.get('end')
                year = data.get('year')

                if year:
                    year = int(year)

                if year is None:
                    year = 2021

                list_data_return = []
                if start and end:
                    start = int(start)
                    end = int(end)

                    list_data_return = format_data_return_day_manager(start, end, record_staff)

                elif not start or not end:
                    for i in range(1, 13):
                        # print(i)
                        data_document = {}
                        data_document_finish = {}
                        data_document_not_finish = {}

                        start = default_timestamp_day_start(year, i)
                        end = default_timestamp_day_end(year, i)

                        sum_document = db.session.query(DepartmentDocument).filter(
                            DepartmentDocument.department_id == record_staff.department_id).filter(
                            DepartmentDocument.created_at >= start if start is not None else True).filter(
                            DepartmentDocument.created_at <= end if end is not None else True).filter(
                            DepartmentDocument.deleted == False).count()
                        # print(sum_document)
                        # print(start)
                        # print(end)
                        data_document["time"] = 'Tháng ' + str(i)
                        data_document["value"] = sum_document
                        data_document["category"] = 'Số lượng văn bản'

                        list_data_return.append(data_document)

                        sum_document_finish = db.session.query(DepartmentDocument).filter(
                            DepartmentDocument.department_id == record_staff.department_id).filter(
                            DepartmentDocument.created_at >= start if start is not None else True).filter(
                            DepartmentDocument.created_at <= end if end is not None else True).filter(
                            DepartmentDocument.status == DocumentStatus.FINISH).filter(
                            DepartmentDocument.deleted == False).count()

                        data_document_finish["time"] = 'Tháng ' + str(i)
                        data_document_finish["value"] = sum_document_finish
                        data_document_finish["category"] = 'Số lượng văn bản hoàn thành'

                        list_data_return.append(data_document_finish)

                        sum_document_not_finish = db.session.query(DepartmentDocument).filter(
                            DepartmentDocument.department_id == record_staff.department_id).filter(
                            DepartmentDocument.created_at >= start if start is not None else True).filter(
                            DepartmentDocument.created_at <= end if end is not None else True).filter(
                            DepartmentDocument.status != DocumentStatus.FINISH).filter(
                            DepartmentDocument.deleted == False).count()

                        data_document_not_finish["time"] = 'Tháng ' + str(i)
                        data_document_not_finish["value"] = sum_document_not_finish
                        data_document_not_finish["category"] = 'Số lượng văn bản chưa hoàn thành'

                        list_data_return.append(data_document_not_finish)

                return format_data_return(response_data={'data': {
                    'result': list_data_return,
                }},
                    response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
