import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.staff import Staff
from application.models.document import Document
from application.models.work_report import WorkReport
from application.models.staff_document import StaffDocument

from application.const import DocumentStatus, ReportStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_staff(request, token: Token):
    object = 'document_staff'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_staff = get_user_info(token)

                records_staff_document = db.session.query(StaffDocument).filter(
                    StaffDocument.staff_id == record_staff.id).filter(
                    StaffDocument.deleted == False).all()
                # print(records_department_document)

                list_id_document = []
                for data_record_staff_document in records_staff_document:
                    list_id_document.append(
                        str(data_record_staff_document.document_id))

                # print(list_id_document)
                records_document = db.session.query(Document).filter(
                    Document.id.in_(list_id_document)).filter(
                    Document.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_document in records_document:
                    list_data_return.append(to_dict(data_record_document))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
