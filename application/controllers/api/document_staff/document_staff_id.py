import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.staff_document import StaffDocument

from application.const import DocumentStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_staff_id(request, token: Token, id):
    object = 'document_staff'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_staff = get_user_info(token)

                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                record_document_return = to_dict(record_document)

                record_staff_document = db.session.query(StaffDocument).filter(
                    StaffDocument.document_id == id).filter(
                    StaffDocument.staff_id == record_staff.id).filter(
                    StaffDocument.deleted == False).first()

                record_document_return['request'] = record_staff_document.description if record_staff_document is not None else None

                return format_data_return(response_data={'data': {
                                                            'result': record_document_return
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            record_staff = get_user_info(token)

            try:
                record_staff_document = db.session.query(StaffDocument).filter(
                    StaffDocument.document_id == id).filter(
                    StaffDocument.staff_id == record_staff.id).filter(
                    StaffDocument.deleted == False).first()

                if not record_staff_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": văn bản không được phân cho nhân viên này",
                                              response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_document.status = DocumentStatus.WORKING
                record_staff_document.status = DocumentStatus.WORKING
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_staff_document)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
