from application.server import app
from application.controllers.api.document_staff.document_staff import document_staff
from application.controllers.api.document_staff.document_staff_id import document_staff_id


app.add_route(document_staff, 'document-staff/api/v1/web/document-staff/',
              methods=['GET'])
app.add_route(document_staff_id, 'document-staff/api/v1/web/document-staff/<id:uuid>',
              methods=['PUT', 'GET'])
