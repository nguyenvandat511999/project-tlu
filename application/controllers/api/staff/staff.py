import traceback
from application.database import db
from sqlalchemy.sql.elements import or_
from gatco_restapi.helpers import to_dict
from application.models.staff import Staff
from application.models.position import Position
from application.models.department import Department
from sqlalchemy import *
from application.common.message import ErrorMessage
from application.controllers.cores.utilities import pagination
from application.controllers.api.staff import data_staff, check_position, data_department, check_department
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.databases.sqlalchemy_function import Unaccent
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_null_and_not_empty, is_valid_email,
    validate_user_login, validate_user
)

from unidecode import unidecode
from sanic_jwt_extended.tokens import Token

from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def staff(request, token: Token):
    object = 'staff'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            user = validate_user(token)

            if user != 0:
                data = request.args
                name = data.get('name')
                position_id = data.get('position_id')
                department_id = data.get('department_id')

                staff = db.session.query(Staff).filter(
                    Staff.id == user.staff_id).filter(
                    Staff.deleted == False).first()

                search_str_unaccent = "%" + unidecode(name.strip().lower()) + "%" if string_is_not_null_and_not_empty(
                    name) else None

                try:
                    record_staff = []
                    # print(check_department(staff))
                    if check_department(staff) == 0:
                        department_id = staff.department_id

                        record_staff = db.session.query(Staff).filter(
                            Staff.deleted == False).filter(
                            or_(Unaccent(Staff.code).ilike(search_str_unaccent),
                                Unaccent(Staff.name).ilike(search_str_unaccent),
                                Unaccent(Staff.email).ilike(search_str_unaccent),
                                ) if search_str_unaccent is not None else True).filter(
                            Staff.position_id == position_id if position_id is not None else True).filter(
                            Staff.department_id == department_id if department_id is not None else True).order_by(
                            Staff.created_at.desc(), Staff.id.desc()).all()

                    elif check_department(staff) == 1:
                        record_staff = db.session.query(Staff).filter(
                            Staff.deleted == False).filter(
                            or_(Unaccent(Staff.code).ilike(search_str_unaccent),
                                Unaccent(Staff.name).ilike(search_str_unaccent),
                                Unaccent(Staff.email).ilike(search_str_unaccent),
                                ) if search_str_unaccent is not None else True).filter(
                            Staff.position_id == position_id if position_id is not None else True).filter(
                            Staff.department_id == department_id if department_id is not None else True).order_by(
                            Staff.created_at.desc(), Staff.id.desc()).all()

                    elif check_department(staff) == 2:
                        department_id = data_department(staff)
                        # print(department_id)
                        record_staff = db.session.query(Staff).filter(
                            Staff.deleted == False).filter(
                            or_(Unaccent(Staff.code).ilike(search_str_unaccent),
                                Unaccent(Staff.name).ilike(search_str_unaccent),
                                Unaccent(Staff.email).ilike(search_str_unaccent),
                                ) if search_str_unaccent is not None else True).filter(
                            Staff.position_id == position_id if position_id is not None else True).filter(
                            Staff.department_id.in_(department_id)).order_by(
                            Staff.created_at.desc(), Staff.id.desc()).all()

                    list_data_return = pagination(request, record_staff)

                    if not list_data_return:
                        return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": limit, offset không đúng",
                                                  response_http_code=400)

                    return format_data_return(response_data={'data': list_data_return},
                                              response_http_code=200)

                except Exception as e:
                    print(traceback.format_exc())
                    return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                              response_http_code=400)

        if method == 'POST':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            name = data.get('name')
            code = data.get('code')
            email = data.get('email')
            status = data.get('status')
            gender = data.get('gender')
            address = data.get('address')
            birthday = data.get('birthday')
            phone_number = data.get('phone_number')
            department_id = data.get('department_id')
            position_id = data.get('position_id')

            if string_is_null_or_empty(name) or string_is_null_or_empty(code):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, code trống",
                                          response_http_code=400)

            if email and not is_valid_email(email):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email không đúng định dạng",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.code == code).filter(
                Staff.deleted == False).first()

            if record_staff:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": code đã tồn tại",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.email == email).filter(
                Staff.deleted == False).first()

            if record_staff:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email đã tồn tại",
                                          response_http_code=400)

            try:
                record_staff = Staff()

                if position_id:
                    record_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    record_staff.position_id = record_position.id
                    record_staff.position_name = record_position.name

                if department_id:
                    record_department = db.session.query(Department).filter(
                        Department.id == department_id).filter(
                        Department.deleted == False).first()

                    record_staff.department_id = record_department.id
                    record_staff.department_name = record_department.name

                record_staff.name = name
                record_staff.code = code
                record_staff.email = email
                record_staff.address = address
                record_staff.phone_number = phone_number
                record_staff.gender = gender
                record_staff.birthday = birthday
                record_staff.status = status

                db.session.add(record_staff)
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_staff)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


async def staff_mobile(request):
    method = request.method
    try:
        if method == 'GET':
            data = request.args
            name = data.get('name')
            position_id = data.get('position_id')
            department_id = data.get('department_id')

            search_str_unaccent = "%" + unidecode(name.strip().lower()) + "%" if string_is_not_null_and_not_empty(
                name) else None

            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.deleted == False).filter(
                    or_(Unaccent(Staff.code).ilike(search_str_unaccent),
                        Unaccent(Staff.name).ilike(
                            search_str_unaccent),
                        Unaccent(Staff.email).ilike(
                            search_str_unaccent),
                        ) if search_str_unaccent is not None else True).filter(
                    Staff.position_id == position_id if position_id is not None else True).filter(
                    Staff.department_id == department_id if department_id is not None else True).order_by(
                    Staff.created_at.desc(), Staff.id.desc()).all()

                list_data_return = pagination(request, record_staff)

                if not list_data_return:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": limit, offset không đúng",
                                              response_http_code=400)

                return format_data_return(response_data={'data': list_data_return},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            data = request.json
            name = data.get('name')
            code = data.get('code')
            email = data.get('email')
            status = data.get('status')
            gender = data.get('gender')
            address = data.get('address')
            birthday = data.get('birthday')
            phone_number = data.get('phone_number')
            department_id = data.get('department_id')
            position_id = data.get('position_id')

            if string_is_null_or_empty(name) or string_is_null_or_empty(code):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, code trống",
                                          response_http_code=400)

            if email and not is_valid_email(email):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email không đúng định dạng",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.code == code).filter(
                Staff.deleted == False).first()

            if record_staff:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": code đã tồn tại",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.email == email).filter(
                Staff.deleted == False).first()

            if record_staff:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email đã tồn tại",
                                          response_http_code=400)

            try:
                record_staff = Staff()

                if position_id:
                    record_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    record_staff.position_id = record_position.id
                    record_staff.position_name = record_position.name

                if department_id:
                    record_department = db.session.query(Department).filter(
                        Department.id == department_id).filter(
                        Department.deleted == False).first()

                    record_staff.department_id = record_department.id
                    record_staff.department_name = record_department.name

                record_staff.name = name
                record_staff.code = code
                record_staff.email = email
                record_staff.address = address
                record_staff.phone_number = phone_number
                record_staff.gender = gender
                record_staff.birthday = birthday
                record_staff.status = status

                db.session.add(record_staff)
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_staff)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
