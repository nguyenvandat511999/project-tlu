import traceback
from application.models.staff import Staff
from application.models.account import Account
from gatco_restapi.helpers import to_dict
from application.database import db
from sqlalchemy import *
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from sanic_jwt_extended.tokens import Token

from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def staff_list(request, token: Token):
    object = 'staff'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)
            try:
                records_account = db.session.query(Account).filter(
                    Account.deleted == False).filter(
                    Account.staff_id != None).all()

                list_id_staff = []
                for data_account in records_account:
                    list_id_staff.append(str(data_account.staff_id))

                records_staff = db.session.query(Staff).filter(
                    Staff.deleted == False).all()
                print(list_id_staff)
                list_data_return = []
                for data_staff in records_staff:
                    if str(data_staff.id) not in list_id_staff:
                        list_data_return.append(to_dict(data_staff))

                return format_data_return(response_data={'data':{
                                                            'result': list_data_return
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


async def staff_list_mobile(request):
    method = request.method
    try:
        if method == 'GET':
            try:
                records_account = db.session.query(Account).filter(
                    Account.deleted == False).filter(
                    Account.staff_id != None).all()

                list_id_staff = []
                for data_account in records_account:
                    list_id_staff.append(str(data_account.staff_id))

                records_staff = db.session.query(Staff).filter(
                    Staff.deleted == False).all()
                print(list_id_staff)
                list_data_return = []
                for data_staff in records_staff:
                    if str(data_staff.id) not in list_id_staff:
                        list_data_return.append(to_dict(data_staff))

                return format_data_return(response_data={'data': {
                    'result': list_data_return
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
