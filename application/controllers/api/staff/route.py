from application.server import app
from application.controllers.api.staff.staff import staff, staff_mobile
from application.controllers.api.staff.staff_id import staff_id, staff_id_mobile
from application.controllers.api.staff.staff_list import staff_list, staff_list_mobile

app.add_route(staff_id, 'staff/api/v1/web/staff-management/<id:uuid>', methods=['GET', 'PUT', 'DELETE'])
app.add_route(staff, 'staff/api/v1/web/staff-management/', methods=['POST', 'GET'])
app.add_route(staff_list, 'staff/api/v1/web/staff-list/', methods=['GET'])

app.add_route(staff_id_mobile, 'staff/api/v1/mobile/staff-management/<id:uuid>', methods=['GET', 'PUT', 'DELETE'])
app.add_route(staff_mobile, 'staff/api/v1/mobile/staff-management/', methods=['POST', 'GET'])
app.add_route(staff_list_mobile, 'staff/api/v1/mobile/staff-list/', methods=['GET'])
