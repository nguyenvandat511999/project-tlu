from application.database import db
from application.models.staff import Staff
from application.models.position import Position
from application.models.department import Department


def data_staff(staff):
    list_position_id = []
    list_position_id = staff_position(str(staff.position_id), list_position_id)
    return list_position_id


def staff_position(position_id, list_position_id):
    position = db.session.query(Position).filter(
        Position.id == position_id).filter(
        Position.deleted == False).first()
    if position.children:
        for data_children in position.children:
            list_position_id.append(str(data_children.id))
            return staff_position(str(data_children.id), list_position_id)
    return list_position_id


def check_position(staff):
    position = db.session.query(Position).filter(
        Position.id == staff.position_id).filter(
        Position.deleted == False).first()

    if position.parent_id == None:
        return 1
    return 0


def data_department(staff):
    list_department_id = []
    list_department_id.append(str(staff.department_id))
    list_department_id = staff_department(str(staff.department_id), list_department_id)
    return list_department_id


def staff_department(department_id, list_department_id):
    department = db.session.query(Department).filter(
        Department.id == department_id).filter(
        Department.deleted == False).first()

    record_data_children = db.session.query(Department).filter(
        Department.parent_id == department.id).filter(
        Department.deleted == False).all()

    if record_data_children:
        for data_children in record_data_children:
            list_department_id.append(str(data_children.id))
            return staff_department(str(data_children.id), list_department_id)
    return list_department_id


def check_department(staff):
    department = db.session.query(Department).filter(
        Department.id == staff.department_id).filter(
        Department.deleted == False).first()

    if department.parent_id == None:
        return 1
    if department.children == None:
        return 0
    return 2
