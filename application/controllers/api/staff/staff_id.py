import traceback
from application.models.staff import Staff
from application.models.position import Position
from application.models.department import Department
from application.models.account import Account
from gatco_restapi.helpers import to_dict
from application.database import db
from sqlalchemy import *
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty, string_is_not_valid_uuid,
    string_is_not_null_and_not_empty, is_valid_email,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token

from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def staff_id(request, id, token: Token):
    object = 'staff'
    method = request.method
    try:
        if method == 'GET':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                if id:
                    record_staff = db.session.query(Staff).filter(
                        Staff.id == id).filter(
                        Staff.deleted == False).first()

                    if not record_staff:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_staff.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": Staff đã bị xóa",
                                                  response_http_code=404)

                    return format_data_return(response_data={'data': {
                        'result': to_dict(record_staff)
                    }},
                        response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            name = data.get('name')
            code = data.get('code')
            email = data.get('email')
            address = data.get('address')
            phone_number = data.get('phone_number')
            gender = data.get('gender')
            birthday = data.get('birthday')
            status = data.get('status')
            position_id = data.get('position_id')
            department_id = data.get('department_id')

            if string_is_null_or_empty(name) or string_is_null_or_empty(code):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, code trống",
                                          response_http_code=400)

            if email and not is_valid_email(email):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email không đúng định dạng",
                                          response_http_code=400)

            if department_id and string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": department_id không đúng định dạng uuid",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.code == code).filter(
                Staff.deleted == False).first()

            if record_staff and record_staff.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": code đã tồn tại",
                                          response_http_code=400)
            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.id == id).filter(
                    Staff.deleted == False).first()

                if position_id:
                    record_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    record_staff.position_id = record_position.id
                    record_staff.position_name = record_position.name

                if department_id:
                    record_department = db.session.query(Department).filter(
                        Department.id == department_id).filter(
                        Department.deleted == False).first()

                    record_staff.department_id = record_department.id
                    record_staff.department_name = record_department.name

                record_staff.name = name
                record_staff.code = code
                record_staff.email = email
                record_staff.address = address
                record_staff.phone_number = phone_number
                record_staff.gender = gender
                record_staff.birthday = birthday
                record_staff.status = status

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_staff)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.id == id).filter(
                    Staff.deleted == False).first()

                if not record_staff:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_staff.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


async def staff_id_mobile(request, id):
    method = request.method
    try:
        if method == 'GET':
            try:
                if id:
                    record_staff = db.session.query(Staff).filter(
                        Staff.id == id).filter(
                        Staff.deleted == False).first()

                    if not record_staff:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_staff.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": Staff đã bị xóa",
                                                  response_http_code=404)

                    return format_data_return(response_data={'data': {
                        'result': to_dict(record_staff)
                    }},
                        response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            data = request.json
            name = data.get('name')
            code = data.get('code')
            email = data.get('email')
            address = data.get('address')
            phone_number = data.get('phone_number')
            gender = data.get('gender')
            birthday = data.get('birthday')
            status = data.get('status')
            position_id = data.get('position_id')
            department_id = data.get('department_id')

            if string_is_null_or_empty(name) or string_is_null_or_empty(code):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, code trống",
                                          response_http_code=400)

            if email and not is_valid_email(email):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": email không đúng định dạng",
                                          response_http_code=400)

            if department_id and string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": department_id không đúng định dạng uuid",
                                          response_http_code=400)

            record_staff = db.session.query(Staff).filter(
                Staff.code == code).filter(
                Staff.deleted == False).first()

            if record_staff and record_staff.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": code đã tồn tại",
                                          response_http_code=400)
            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.id == id).filter(
                    Staff.deleted == False).first()

                if position_id:
                    record_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    record_staff.position_id = record_position.id
                    record_staff.position_name = record_position.name

                if department_id:
                    record_department = db.session.query(Department).filter(
                        Department.id == department_id).filter(
                        Department.deleted == False).first()

                    record_staff.department_id = record_department.id
                    record_staff.department_name = record_department.name

                record_staff.name = name
                record_staff.code = code
                record_staff.email = email
                record_staff.address = address
                record_staff.phone_number = phone_number
                record_staff.gender = gender
                record_staff.birthday = birthday
                record_staff.status = status

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_staff)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.id == id).filter(
                    Staff.deleted == False).first()

                if not record_staff:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_staff.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
