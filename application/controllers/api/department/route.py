from application.server import app
from application.controllers.api.department.department import department, department_mobile
from application.controllers.api.department.department_id import department_id, department_id_mobile
from application.controllers.api.department.department_tree import department_tree, department_tree_mobile
from application.controllers.api.department.department_list import department_list

app.add_route(department_id, 'department/api/v1/web/department-management/<id:uuid>', methods=['GET', 'PUT', 'DELETE'])
app.add_route(department, 'department/api/v1/web/department-management/', methods=['POST', 'GET'])
app.add_route(department_tree, 'department/api/v1/web/department-tree/', methods=['GET'])
app.add_route(department_list, 'department/api/v1/web/department-list/', methods=['GET'])

app.add_route(department_id_mobile, 'department/api/v1/mobile/department-management/<id:uuid>', methods=['GET', 'PUT', 'DELETE'])
app.add_route(department_mobile, 'department/api/v1/mobile/department-management/', methods=['POST', 'GET'])
app.add_route(department_tree_mobile, 'department/api/v1/mobile/department-tree/', methods=['GET'])
