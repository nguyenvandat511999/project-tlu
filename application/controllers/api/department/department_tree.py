import traceback
from sqlalchemy import *

from application.database import db
from application.models.department import Department
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.api.department.func_department import format_department_return

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def department_tree(request, token: Token):
    try:
        if request.method == 'GET':
            try:
                records_department = db.session.query(Department).filter(
                    Department.deleted == False).filter(
                    Department.parent_id == None).first()

                list_data_return = []
                if records_department:
                    list_data_return = format_department_return(records_department)
                    num_result = db.session.query(Department).filter(
                        Department.deleted == False).count()

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


async def department_tree_mobile(request):
    try:
        if request.method == 'GET':
            try:
                records_department = db.session.query(Department).filter(
                    Department.deleted == False).filter(
                    Department.parent_id == None).first()

                list_data_return = []
                if records_department:
                    list_data_return = format_department_return(
                        records_department)
                    num_result = db.session.query(Department).filter(
                        Department.deleted == False).count()

                return format_data_return(response_data={'data': {
                    'result': list_data_return,
                    'total': num_result
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
