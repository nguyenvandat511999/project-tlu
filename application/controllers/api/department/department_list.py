import traceback
from sqlalchemy import *

from application.database import db
from gatco_restapi.helpers import to_dict
from application.models.department import Department
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return


from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def department_list(request, token: Token):
    object = 'department_list'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                records_department = db.session.query(Department).filter(
                    Department.parent_id != None).filter(
                    Department.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_department in records_department:
                    list_data_return.append(to_dict(data_record_department))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
