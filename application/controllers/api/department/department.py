import traceback
from gatco_restapi.helpers import to_dict
from sqlalchemy import *

from application.database import db
from application.models.department import Department
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def department(request, token: Token):
    object = 'department'
    method = request.method
    try:
        if method == 'GET':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                records_department = db.session.query(Department).filter(
                    Department.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_department in records_department:
                    list_data_return.append(to_dict(data_record_department))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            name = data.get('name')
            created_at = data.get('created_at')
            description = data.get('description')
            department_id = data.get('parent_id')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            record_department = db.session.query(Department).filter(
                Department.name == name).filter(
                Department.deleted == False).first()

            if record_department:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            if department_id and string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": parent_id không đúng định dạng uuid",
                                          response_http_code=400)

            if department_id:
                record_parent_department = db.session.query(Department).filter(
                    Department.id == department_id).filter(
                    Department.deleted == False).first()

                if not record_parent_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy department id",
                                              response_http_code=404)

            try:
                record_department = Department()

                record_department.name = name
                record_department.description = description
                record_department.created_at = created_at

                if department_id:
                    record_department.parent_id = record_parent_department.id
                    record_department.parent_name = record_parent_department.name

                db.session.add(record_department)
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_department)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


async def department_mobile(request):
    method = request.method
    try:
        if method == 'GET':
            try:
                records_department = db.session.query(Department).filter(
                    Department.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_department in records_department:
                    list_data_return.append(to_dict(data_record_department))
                    num_result += 1

                return format_data_return(response_data={'data': {
                    'result': list_data_return,
                    'total': num_result
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            data = request.json
            name = data.get('name')
            created_at = data.get('created_at')
            description = data.get('description')
            department_id = data.get('parent_id')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            record_department = db.session.query(Department).filter(
                Department.name == name).filter(
                Department.deleted == False).first()

            if record_department:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            if department_id and string_is_not_valid_uuid(department_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": parent_id không đúng định dạng uuid",
                                          response_http_code=400)

            if department_id:
                record_parent_department = db.session.query(Department).filter(
                    Department.id == department_id).filter(
                    Department.deleted == False).first()

                if not record_parent_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy department id",
                                              response_http_code=404)

            try:
                record_department = Department()

                record_department.name = name
                record_department.description = description
                record_department.created_at = created_at

                if department_id:
                    record_department.parent_id = record_parent_department.id
                    record_department.parent_name = record_parent_department.name

                db.session.add(record_department)
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_department)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
