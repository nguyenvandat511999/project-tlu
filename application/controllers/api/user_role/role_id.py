import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.account import Account
from application.models.user_role import UserRole
from application.models.permission import Permission
from application.models.user_role_permission import UserRolePermission

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    validate_user_login
)
from application.controllers.api.user_role.func import format_data_role_return

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def role_id(request, id, token: Token):
    object = 'role'
    method = request.method
    try:
        if method == 'GET':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                if id:
                    record_role = db.session.query(UserRole).filter(
                        UserRole.id == id).filter(
                        UserRole.deleted == False).first()

                    if not record_role:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_role.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + " role đã bị xóa",
                                                  response_http_code=404)

                    data_return = format_data_role_return(record_role)

                    return format_data_return(response_data={'data': {
                                                                'result': data_return
                                                            }},
                                              response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            name = data.get('name')
            permission = data.get('permission')
            account_id = data.get('account_id')
            description = data.get('description')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            if account_id and list_string_is_not_valid_uuid(account_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": account_id sai uuid",
                                          response_http_code=400)

            if list_string_is_not_valid_uuid(permission):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": permission sai uuid",
                                          response_http_code=400)

            record_role = db.session.query(UserRole).filter(
                UserRole.name == name).filter(
                UserRole.deleted == False).first()

            if record_role and record_role.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            try:
                record_role = db.session.query(UserRole).filter(
                    UserRole.id == id).filter(
                    UserRole.deleted == False).first()

                record_role.name = name
                record_role.description = description

                if account_id:
                    records_account = db.session.query(Account).filter(
                        Account.id.in_(account_id)).filter(
                        Account.deleted == False).all()

                    for data_account in records_account:
                        data_account.role_id = record_role.id
                        data_account.role_name = record_role.name

                records_role_permission = db.session.query(UserRolePermission).filter(
                    UserRolePermission.user_role_id == id).filter(
                    UserRolePermission.deleted == False).all()

                list_role_permission = []
                for data_role_permission in records_role_permission:
                    list_role_permission.append(str(data_role_permission.permission_id))

                for data_permission in permission:
                    if data_permission not in list_role_permission:
                        record_permission = db.session.query(Permission).filter(
                            Permission.id == data_permission).filter(
                            Permission.deleted == False).first()

                        record_role_permission = UserRolePermission()
                        record_role_permission.user_role_id = record_role.id
                        record_role_permission.permission_id = record_permission.id

                        record_permission.user_role.append(record_role_permission)
                        record_role.permission.append(record_role_permission)

                for id_role_permission in list_role_permission:
                    if id_role_permission not in permission:
                        record_role_permission = db.session.query(UserRolePermission).filter(
                            UserRolePermission.user_role_id == id).filter(
                            UserRolePermission.permission_id == id_role_permission).filter(
                            UserRolePermission.deleted == False).first()

                        record_role_permission.deleted = True

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_role)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                record_role = db.session.query(UserRole).filter(
                    UserRole.id == id).filter(
                    UserRole.deleted == False).first()

                if not record_role:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_role.deleted = True

                records_role_permission = db.session.query(UserRolePermission).filter(
                    UserRolePermission.user_role_id == id).filter(
                    UserRolePermission.deleted == False).all()

                if records_role_permission:
                    for data_record in records_role_permission:
                        data_record.deleted = True

                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
