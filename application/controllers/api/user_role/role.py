from application.controllers.api.permission.permission import permission
import traceback
from sqlalchemy import *

from application.database import db
from gatco_restapi.helpers import to_dict
from application.models.account import Account
from application.models.user_role import UserRole
from application.common.message import ErrorMessage
from application.models.permission import Permission
from application.models.user_role_permission import UserRolePermission
from application.controllers.cores.http_client import format_data_return
from application.controllers.api.user_role.func import format_data_role_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_not_valid_uuid,
    string_is_null_or_empty,
    validate_user_login
)
from application.controllers.cores.utilities.convert_data import (
    convert_list_to_string
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def role(request, token: Token):
    object = 'role'
    method = request.method
    try:
        if method == 'GET':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                records_role = db.session.query(UserRole).filter(
                    UserRole.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_role in records_role:
                    data_return = format_data_role_return(data_record_role)
                    list_data_return.append(data_return)
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            name = data.get('name')
            permission = data.get('permission')
            account_id = data.get('account_id')
            description = data.get('description')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            if account_id and list_string_is_not_valid_uuid(account_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": account_id sai uuid",
                                          response_http_code=400)

            if list_string_is_not_valid_uuid(permission):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": permission sai uuid",
                                          response_http_code=400)

            record_role = db.session.query(UserRole).filter(
                UserRole.name == name).filter(
                UserRole.deleted == False).first()

            if record_role:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            try:
                record_role = UserRole()

                record_role.name = name
                record_role.description = description

                db.session.add(record_role)
                db.session.flush()

                if account_id:
                    records_account = db.session.query(Account).filter(
                        Account.id.in_(account_id)).filter(
                        Account.deleted == False).all()

                    for data_account in records_account:
                        data_account.role_id = record_role.id
                        data_account.role_name = record_role.name

                records_permission = db.session.query(Permission).filter(
                    Permission.id.in_(permission)).filter(
                    Permission.deleted == False).all()

                for data_permission in records_permission:

                    record_role_permission = UserRolePermission()
                    record_role_permission.user_role_id = record_role.id
                    record_role_permission.permission_id = data_permission.id

                    data_permission.user_role.append(record_role_permission)
                    record_role.permission.append(record_role_permission)

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_role)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
