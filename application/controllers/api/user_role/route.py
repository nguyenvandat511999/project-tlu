from application.server import app
from application.controllers.api.user_role.role import role
from application.controllers.api.user_role.role_id import role_id

app.add_route(role_id, 'role/api/v1/web/role-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(role, 'role/api/v1/web/role-management/',
              methods=['POST', 'GET'])
