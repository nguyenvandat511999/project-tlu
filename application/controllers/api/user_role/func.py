from application.database import db
from gatco_restapi.helpers import to_dict
from application.models.user_role_permission import UserRolePermission


def format_data_role_return(record_role):
    list_id_permission = []
    records_role_permission = db.session.query(UserRolePermission).filter(
        UserRolePermission.user_role_id == record_role.id).filter(
        UserRolePermission.deleted == False).all()

    for data_permission in records_role_permission:
        list_id_permission.append(str(data_permission.permission_id))

    data_return = to_dict(record_role)
    data_return['permission'] = list_id_permission

    return data_return
