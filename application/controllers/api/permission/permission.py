import traceback
from sqlalchemy import *

from application.database import db
from gatco_restapi.helpers import to_dict
from application.models.account import Account
from application.common.message import ErrorMessage
from application.models.permission import Permission
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def permission(request, token: Token):
    try:
        if request.method == 'GET':
            try:
                records_permission = db.session.query(Permission).filter(
                    Permission.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_permission in records_permission:
                    list_data_return.append(to_dict(data_record_permission))
                    num_result += 1

                return format_data_return(response_data={'data': {
                    'result': list_data_return,
                    'total': num_result
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'POST':
            data = request.json
            permission = data.get('permission')

            if not permission:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": permission trống",
                                          response_http_code=400)

            try:
                for data_permission in permission:
                    name = data_permission['name']
                    type = data_permission['type']
                    method = data_permission['method']

                    if string_is_null_or_empty(name):
                        return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                                  response_http_code=400)

                    if string_is_null_or_empty(type):
                        return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": type trống",
                                                  response_http_code=400)

                    record_permission = db.session.query(Permission).filter(
                        Permission.name == name).filter(
                        Permission.deleted == False).first()

                    if not record_permission:
                        record_permission = Permission()

                        record_permission.name = name
                        record_permission.type = type
                        record_permission.method = method

                        db.session.add(record_permission)

                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
