from application.server import app
from application.controllers.api.permission.permission import permission
from application.controllers.api.permission.permission_id import permission_id

app.add_route(permission_id, 'permission/api/v1/web/permission-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(permission, 'permission/api/v1/web/permission-management/',
              methods=['POST', 'GET'])
