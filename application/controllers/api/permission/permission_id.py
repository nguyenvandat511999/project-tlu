import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.account import Account
from application.models.permission import Permission

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def permission_id(request, id, token: Token):
    try:
        if request.method == 'GET':
            try:
                if id:
                    record_permission = db.session.query(Permission).filter(
                        Permission.id == id).filter(
                        Permission.deleted == False).first()

                    if not record_permission:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_permission.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + " permission đã bị xóa",
                                                  response_http_code=404)

                    return format_data_return(response_data={'data': {
                        'result': to_dict(record_permission)
                    }},
                        response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'PUT':
            data = request.json
            name = data.get('name')
            type = data.get('type')
            method = data.get('method')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            if string_is_null_or_empty(type):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": type trống",
                                          response_http_code=400)

            record_permission = db.session.query(Permission).filter(
                or_(Permission.name == name,
                    Permission.type == type)).filter(
                Permission.deleted == False).first()

            if record_permission and record_permission.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, type đã tồn tại",
                                          response_http_code=400)

            try:
                record_permission = db.session.query(Permission).filter(
                    Permission.id == id).filter(
                    Permission.deleted == False).first()

                record_permission.name = name
                record_permission.type = type
                record_permission.method = method

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_permission)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'DELETE':
            try:
                record_permission = db.session.query(Permission).filter(
                    Permission.id == id).filter(
                    Permission.deleted == False).first()

                if not record_permission:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_permission.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
