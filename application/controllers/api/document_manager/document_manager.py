import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.staff import Staff
from application.models.document import Document
from application.models.staff_document import StaffDocument
from application.models.department_document import DepartmentDocument

from application.const import DocumentStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_manager(request, token: Token):
    object = 'document_manager'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_staff = get_user_info(token)

                department_id = record_staff.department_id

                records_department_document = db.session.query(DepartmentDocument).filter(
                    DepartmentDocument.department_id == department_id).filter(
                    DepartmentDocument.deleted == False).all()
                # print(records_department_document)

                list_id_document = []
                for data_record_department_document in records_department_document:
                    list_id_document.append(str(data_record_department_document.document_id))
                # print(list_id_document)

                list_data_return = []
                num_result = 0
                for data_id in list_id_document:
                    record_document = db.session.query(Document).filter(
                        Document.id == data_id).filter(
                        Document.deleted == False).first()

                    record_document_return = to_dict(record_document)

                    records_staff_document = db.session.query(StaffDocument).filter(
                        StaffDocument.document_id == data_id).filter(
                        StaffDocument.deleted == False).all()

                    # print(records_staff_document)

                    for data_records_staff_document in records_staff_document:
                        record_staff = db.session.query(Staff).filter(
                            Staff.id == data_records_staff_document.staff_id).filter(
                            Staff.department_id == department_id).filter(
                            Staff.deleted == False).first()

                        # print(record_staff)
                        record_document_return['staff_work'] = record_staff.name if record_staff is not None else None

                    list_data_return.append(record_document_return)
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            staff_id = data.get('staff_id')
            description = data.get('description')
            document_id = data.get('document_id')

            if string_is_not_valid_uuid(document_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_id không đúng định dạng uuid",
                                          response_http_code=400)

            if string_is_not_valid_uuid(staff_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": staff_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_staff = db.session.query(Staff).filter(
                    Staff.id == staff_id).filter(
                    Staff.deleted == False).first()

                if not record_staff:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                              response_http_code=404)

                record_staff_document = db.session.query(StaffDocument).filter(
                    StaffDocument.document_id == document_id).filter(
                    StaffDocument.staff_id == staff_id).filter(
                    StaffDocument.deleted == False).first()

                if record_staff_document:
                    return format_data_return(response_message=ErrorMessage.DATA_IS_EXISTS + ": văn bản đã tồn tại",
                                              response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == document_id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_staff_document = StaffDocument()

                record_staff_document.description = description
                record_staff_document.staff_id = record_staff.id
                record_staff_document.staff_name = record_staff.name
                record_staff_document.document_id = record_document.id
                record_staff_document.document_name = record_document.name

                record_staff.document.append(record_staff_document)
                record_document.staff.append(record_staff_document)
                record_document.status = DocumentStatus.RECEIVE

                db.session.add(record_staff_document)
                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_staff_document)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
