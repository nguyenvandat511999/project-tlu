from application.server import app
from application.controllers.api.document_manager.document_manager import document_manager
from application.controllers.api.document_manager.document_manager_id import document_manager_id


app.add_route(document_manager, 'document-manager/api/v1/web/document-manager/',
              methods=['POST', 'GET'])
app.add_route(document_manager_id, 'document-manager/api/v1/web/document-manager/<id:uuid>',
              methods=['PUT', 'GET'])
