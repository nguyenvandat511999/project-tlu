import traceback

from unidecode import unidecode
from sqlalchemy.sql.elements import or_
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.account import Account
from application.controllers.cores.utilities import pagination

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from application.controllers.cores.utilities.databases.sqlalchemy_function import Unaccent
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_not_null_and_not_empty,
    string_is_not_valid_uuid
)

from sanic_jwt_extended import (
    jwt_required
)


async def account_list(request):
    try:
        if request.method == 'GET':
            data = request.args
            name = data.get('name')

            search_str_unaccent = "%" + unidecode(name.strip().lower()) + "%" if string_is_not_null_and_not_empty(
                name) else None

            try:
                records_account = db.session.query(Account).filter(
                    Account.deleted == False).filter(
                    or_(Unaccent(Account.username).ilike(search_str_unaccent))
                        if search_str_unaccent is not None else True).filter(
                    Account.role_id == None).order_by(
                    Account.created_at.desc(), Account.id.desc()).all()

                list_data_return = pagination(request, records_account)

                if not list_data_return:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": limit, offset không đúng",
                                              response_http_code=400)

                return format_data_return(response_data={'data': list_data_return},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
