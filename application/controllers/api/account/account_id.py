import traceback

from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.staff import Staff
from application.models.account import Account
from application.models.user_role import UserRole

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from application.controllers.api.account.func_account import hash_password
from application.controllers.cores.utilities.check_data import (
    string_is_not_null_and_not_empty, string_is_not_valid_uuid
)

from sanic_jwt_extended import (
    jwt_required
)


async def account_id(request, id):
    try:
        if request.method == 'GET':
            record_user = db.session.query(Account).filter(
                Account.id == id).filter(
                Account.deleted == False).first()

            if not record_user:
                return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": id không tồn tại",
                                          response_http_code=404)

            try:
                data_user_return = to_dict(record_user)
                del data_user_return['password']

                return format_data_return(response_data={'data': {
                    'result': data_user_return
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'PUT':
            data = request.json
            new_password = data.get('new_password')
            retype_password = data.get('retype_password')
            role_id = data.get('role_id')

            record_user = db.session.query(Account).filter(
                Account.id == id).filter(
                Account.deleted == False).first()

            if not record_user:
                return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": id không tồn tại",
                                          response_http_code=404)

            if new_password and retype_password and new_password != retype_password:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": new password, retype password không khớp",
                                          response_http_code=400)

            try:
                if new_password:
                    password = hash_password(new_password)
                    record_user.password = password

                if role_id:
                    record_user_role = db.session.query(UserRole).filter(
                        UserRole.id == role_id).filter(
                        UserRole.deleted == False).first()

                    record_user.role_id = record_user_role.id
                    record_user.role_name = record_user_role.name

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_user)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'DELETE':
            try:
                record_user = db.session.query(Account).filter(
                    Account.id == id).filter(
                    Account.deleted == False).first()

                if not record_user:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": id không tồn tại",
                                              response_http_code=404)

                record_user.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': []
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
