import traceback

from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.staff import Staff
from application.models.account import Account
from application.models.user_role import UserRole

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from application.controllers.api.account.func_account import check_password, hash_password
from application.controllers.cores.utilities.check_data import (
    validate_user
)
from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def profile(request, token: Token):
    user = validate_user(token)
    try:
        if request.method == 'GET':

            staff = db.session.query(Staff).filter(
                Staff.id == user.staff_id).filter(
                Staff.deleted == False).first()

            try:
                data_user_return = to_dict(staff)

                return format_data_return(response_data={'data': {
                    'result': data_user_return
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if request.method == 'PUT':
            data = request.json
            old_password = data.get('old_password')
            new_password = data.get('new_password')
            retype_password = data.get('retype_password')

            if new_password and retype_password and new_password != retype_password:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": new password, retype password không khớp",
                                          response_http_code=400)

            try:
                if old_password and check_password(old_password, user.password):
                    if new_password:
                        password = hash_password(new_password)
                        user.password = password

                    db.session.commit()
                    data_return = to_dict(user)
                    del data_return['password']
                    return format_data_return(response_data={'data': {
                                                                'result': data_return
                                                            }},
                                              response_http_code=200)

                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": old_password sai",
                                          response_http_code=400)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
