import traceback


from application.server import app
from application.database import db
from gatco_restapi.helpers import to_dict
from application.models.staff import Staff
from application.models.account import Account
from application.models.user_role import UserRole
from application.models.permission import Permission
from application.models.user_role_permission import UserRolePermission

import datetime

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.api.account.func_account import check_password

from sanic_jwt_extended import (
    JWTManager,
    create_access_token,
)

app.config["JWT_SECRET_KEY"] = "super-secret"
JWTManager(app)


async def login(request):
    try:
        data = request.json
        username = data.get('username')
        password = data.get('password')

        user = db.session.query(Account).filter(
                Account.username == username).first()

        if not user:
            return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy user",
                                      response_http_code=404)

        if check_password(password, user.password):

            expires = datetime.timedelta(days=60)
            access_token = await create_access_token(identity=username, app=request.app, expires_delta=expires)

            record_role = db.session.query(UserRole).filter(
                UserRole.id == user.role_id).filter(
                UserRole.deleted == False).first()

            user_data = to_dict(user)
            list_permission = []
            if record_role:
                records_user_permission = db.session.query(UserRolePermission).filter(
                    UserRolePermission.user_role_id == record_role.id).filter(
                    UserRolePermission.deleted == False).all()

                for data_user_permission in records_user_permission:
                    record_permission = db.session.query(Permission).filter(
                        Permission.id == data_user_permission.permission_id).filter(
                        Permission.deleted == False).first()

                    list_permission.append(str(record_permission.id))

                user_data['permission'] = list_permission
                del user_data['password']

            return format_data_return(response_data={'data': {
                                                        'result': user_data,
                                                        'access_token': access_token
                                                    }},
                                      response_http_code=200)

        return format_data_return(response_message=ErrorMessage.PARAM_ERROR,
                                  response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
