from application.server import app
from application.controllers.api.account.login import login
from application.controllers.api.account.account import account
from application.controllers.api.account.profile import profile
from application.controllers.api.account.account_id import account_id
from application.controllers.api.account.account_list import account_list

app.add_route(login, 'account/api/v1/web/login', methods=['POST'])
app.add_route(account_list, 'account/api/v1/web/account-list', methods=['GET'])
app.add_route(account, 'account/api/v1/web/account-management', methods=['POST', 'GET'])
app.add_route(account_id, 'account/api/v1/web/account-management/<id:uuid>', methods=['PUT', 'GET', 'DELETE'])
app.add_route(profile, 'account/api/v1/web/profile', methods=['GET', 'PUT'])
