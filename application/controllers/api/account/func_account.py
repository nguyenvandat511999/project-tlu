import bcrypt
import string
import random


def check_password(current_passw, user_passw):
    current_password = current_passw.encode('utf-8')
    user_password = user_passw.encode('utf-8')
    if bcrypt.checkpw(current_password, user_password):
        return True
    return False


def generate_password(lenght, email):
    letter_and_digits = string.ascii_letters + string.digits
    passw = ''.join(random.choice(letter_and_digits) for i in range(lenght))
    print(passw)
    password = bcrypt.hashpw(bytes(passw, 'utf-8'), bcrypt.gensalt())
    return password.decode("utf-8")


def hash_password(password):
    password = bcrypt.hashpw(bytes(password, 'utf-8'), bcrypt.gensalt())
    return password.decode("utf-8")
