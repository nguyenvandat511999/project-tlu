import traceback

from unidecode import unidecode
from sqlalchemy.sql.elements import or_
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.staff import Staff
from application.models.account import Account
from application.models.user_role import UserRole
from application.controllers.cores.utilities import pagination

from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return

from application.controllers.cores.utilities.databases.sqlalchemy_function import Unaccent
from application.controllers.api.account.func_account import hash_password
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_not_null_and_not_empty,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def account(request, token: Token):
    object = 'account'
    method = request.method
    password_default = '123456'
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.args
            name = data.get('name')

            search_str_unaccent = "%" + unidecode(name.strip().lower()) + "%" if string_is_not_null_and_not_empty(
                name) else None

            try:
                records_account = db.session.query(Account).filter(
                    Account.deleted == False).filter(
                    or_(Unaccent(Account.username).ilike(search_str_unaccent),
                        Unaccent(Account.staff_code).ilike(
                            search_str_unaccent) if Account.staff_code is not None else True,
                        Unaccent(Account.staff_name).ilike(
                            search_str_unaccent) if Account.staff_name is not None else True,
                        ) if search_str_unaccent is not None else True).order_by(
                    Account.created_at.desc(), Account.id.desc()).all()

                list_data_return = pagination(request, records_account)

                if not list_data_return:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": limit, offset không đúng",
                                              response_http_code=400)

                return format_data_return(response_data={'data': list_data_return},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            staff_id = data.get('staff_id')
            role_id = data.get('role_id')

            if list_string_is_not_valid_uuid(staff_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": staff_id không đúng",
                                          response_http_code=400)

            records_staff = db.session.query(Staff).filter(
                Staff.id.in_(staff_id)).filter(
                Staff.deleted == False).all()

            if not records_staff:
                return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff_id không tồn tại",
                                          response_http_code=404)

            try:
                if role_id:
                    record_user_role = db.session.query(UserRole).filter(
                        UserRole.id == role_id).filter(
                        UserRole.deleted == False).first()

                list_account = []
                for data_staff in records_staff:
                    record_account = Account()
                    record_account.username = data_staff.email
                    record_account.password = hash_password(password_default)

                    record_account.staff_id = data_staff.id
                    record_account.staff_name = data_staff.name
                    record_account.staff_code = data_staff.code

                    if role_id:
                        record_account.role_id = record_user_role.id
                        record_account.role_name = record_user_role.name

                    list_account.append(record_account)

                db.session.add_all(list_account)
                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_account)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
