import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.work_report import WorkReport

from application.const import ReportStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_not_valid_uuid,
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def staff_report_id(request, token: Token, id):
    object = 'staff_report'
    method = request.method

    record_staff = record_staff = get_user_info(token)

    if not record_staff:
        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                  response_http_code=404)

    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_work_report),
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            name = data.get('name')
            description = data.get('description')
            document_id = data.get('document_id')

            if string_is_not_valid_uuid(document_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == document_id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_work_report.name = name
                record_work_report.description = description
                record_work_report.staff_id = record_staff.id
                record_work_report.staff_name = record_staff.name
                record_work_report.staff_code = record_staff.code
                record_work_report.document_id = record_document.id
                record_work_report.document_name = record_document.name
                record_work_report.department_id = record_staff.department_id
                record_work_report.department_name = record_staff.department_name
                record_work_report.status = ReportStatus.REVIEW

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_work_report)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


@jwt_required
async def staff_report(request, token: Token):
    object = 'staff_report'
    method = request.method

    record_staff = record_staff = get_user_info(token)

    if not record_staff:
        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                  response_http_code=404)

    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                data = request.args
                status = data.get('status')

                records_work_report = db.session.query(WorkReport).filter(
                    WorkReport.staff_id == record_staff.id).filter(
                    WorkReport.status == status if status is not None else True).filter(
                    WorkReport.deleted == False).all()

                list_data_return = []
                num_result = 0
                if records_work_report:
                    for data_record_report in records_work_report:
                        list_data_return.append(to_dict(data_record_report))
                        num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)
        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            name = data.get('name')
            description = data.get('description')
            document_id = data.get('document_id')

            if string_is_not_valid_uuid(document_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_document = db.session.query(Document).filter(
                    Document.id == document_id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_work_report = WorkReport()

                record_work_report.name = name
                record_work_report.description = description
                record_work_report.staff_id = record_staff.id
                record_work_report.staff_name = record_staff.name
                record_work_report.staff_code = record_staff.code
                record_work_report.document_id = record_document.id
                record_work_report.document_name = record_document.name
                record_work_report.department_id = record_staff.department_id
                record_work_report.department_name = record_staff.department_name
                record_work_report.status = ReportStatus.REVIEW

                db.session.add(record_work_report)
                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_work_report)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
