from application.server import app
from application.controllers.api.document_report.admin_report import admin_report, admin_report_id
from application.controllers.api.document_report.manager_report import manager_report, manager_report_id
from application.controllers.api.document_report.staff_report import staff_report, staff_report_id


app.add_route(admin_report, 'document-report/api/v1/web/admin-report/',
              methods=['GET'])
app.add_route(admin_report_id, 'document-report/api/v1/web/admin-report/<id:uuid>',
              methods=['PUT', 'GET'])

app.add_route(manager_report, 'document-report/api/v1/web/manager-report/',
              methods=['GET'])
app.add_route(manager_report_id, 'document-report/api/v1/web/manager-report/<id:uuid>',
              methods=['GET', 'PUT'])

app.add_route(staff_report, 'document-report/api/v1/web/staff-report/',
              methods=['GET', 'POST'])
app.add_route(staff_report_id, 'document-report/api/v1/web/staff-report/<id:uuid>',
              methods=['PUT', 'GET'])
