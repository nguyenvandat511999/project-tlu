import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.department import Department
from application.models.work_report import WorkReport
from application.models.staff_document import StaffDocument

from application.common.message import ErrorMessage
from application.const import DocumentStatus, ReportStatus
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    validate_user_login,
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def admin_report_id(request, token: Token, id):
    object = 'admin_report'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_department = db.session.query(Department).filter(
                    Department.parent_id == None).filter(
                    Department.deleted == False).first()

                if not record_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": department không tồn tại",
                                              response_http_code=404)

                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_work_report),
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            reject = data.get('reject')
            approve = data.get('approve')
            review_description = data.get('review_description')

            try:
                record_staff = record_staff = get_user_info(token)

                if not record_staff:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                              response_http_code=404)

                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                if reject:
                    record_work_report.review_description = review_description
                    record_work_report.status = ReportStatus.REJECT

                elif approve:
                    record_work_report.review_description = review_description
                    record_work_report.status = ReportStatus.FINISH

                    record_staff_document = db.session.query(StaffDocument).filter(
                        StaffDocument.staff_id == record_work_report.staff_id).filter(
                        StaffDocument.document_id == record_work_report.document_id).filter(
                        StaffDocument.deleted == False).first()

                    record_document = db.session.query(Document).filter(
                        Document.id == record_work_report.document_id).filter(
                        Document.deleted == False).first()

                    if record_document:
                        record_document.status = DocumentStatus.FINISH
                    if record_staff_document:
                        record_staff_document.status = DocumentStatus.FINISH

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_work_report)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


@jwt_required
async def admin_report(request, token: Token):
    object = 'admin_report'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                records_work_report = db.session.query(WorkReport).filter(
                    WorkReport.status != ReportStatus.REJECT).filter(
                    WorkReport.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_report in records_work_report:
                    list_data_return.append(to_dict(data_record_report))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
