import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.work_report import WorkReport

from application.const import ReportStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    get_user_info
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def manager_report_id(request, token: Token, id):
    object = 'manager_report'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_staff = get_user_info(token)

                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.department_id == record_staff.department_id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_work_report),
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            reject = data.get('reject')
            approve = data.get('approve')
            review_description = data.get('review_description')

            try:
                record_staff = record_staff = get_user_info(token)

                if not record_staff:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": staff không tồn tại",
                                              response_http_code=404)

                record_work_report = db.session.query(WorkReport).filter(
                    WorkReport.id == id).filter(
                    WorkReport.department_id == record_staff.department_id).filter(
                    WorkReport.deleted == False).first()

                if not record_work_report:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": báo cáo công việc không tồn tại",
                                              response_http_code=404)

                if reject:
                    record_work_report.review_description = review_description
                    record_work_report.status = ReportStatus.REJECT

                elif approve:
                    record_work_report.review_description = review_description
                    record_work_report.status = ReportStatus.APPROVE

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_work_report)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()


@jwt_required
async def manager_report(request, token: Token):
    object = 'manager_report'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                data = request.args
                status = data.get('status')

                record_staff = get_user_info(token)

                records_work_report = db.session.query(WorkReport).filter(
                    WorkReport.department_id == record_staff.department_id).filter(
                    WorkReport.status == status if status is not None else True).filter(
                    WorkReport.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_report in records_work_report:
                    list_data_return.append(to_dict(data_record_report))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
