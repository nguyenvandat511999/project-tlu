from application.server import app
from application.controllers.api.unit.unit import unit
from application.controllers.api.unit.unit_id import unit_id

app.add_route(unit_id, 'unit/api/v1/web/unit-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(unit, 'unit/api/v1/web/unit-management/',
              methods=['POST', 'GET'])
