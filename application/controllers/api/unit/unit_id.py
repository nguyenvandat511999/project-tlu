import traceback
from gatco_restapi.helpers import to_dict
from sqlalchemy import *

from application.database import db
from application.models.unit import Unit
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def unit_id(request, id, token: Token):
    object = 'unit'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                if id:
                    record_unit = db.session.query(Unit).filter(
                        Unit.id == id).filter(
                        Unit.deleted == False).first()

                    if not record_unit:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_unit.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + " document_type đã bị xóa",
                                                  response_http_code=404)

                    return format_data_return(response_data={'data': {
                        'result': to_dict(record_unit)
                    }},
                        response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            name = data.get('name')
            description = data.get('description')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            record_unit = db.session.query(Unit).filter(
                Unit.name == name).filter(
                Unit.deleted == False).first()

            if record_unit and record_unit.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            try:
                record_unit = db.session.query(Unit).filter(
                    Unit.id == id).filter(
                    Unit.deleted == False).first()

                record_unit.name = name
                record_unit.description = description

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_unit)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_unit = db.session.query(Unit).filter(
                    Unit.id == id).filter(
                    Unit.deleted == False).first()

                if not record_unit:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_unit.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
