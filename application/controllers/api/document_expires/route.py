from sqlalchemy.ext.declarative import api
from application.server import app
from application.controllers.api.document_expires.expires import expires
from application.controllers.api.document_expires.expires_id import expires_id
from application.controllers.api.document_expires.document_expires import document_expires
from application.controllers.api.document_expires.update_document_expires import update_document_expires

app.add_route(expires_id, 'expires-document/api/v1/web/expires/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])

app.add_route(expires, 'expires-document/api/v1/web/expires/',
              methods=['POST', 'GET'])

app.add_route(document_expires, 'expires-document/api/v1/web/document/',
              methods=['GET'])

app.add_route(update_document_expires, 'expires-document/api/v1/web/update-document/',
              methods=['PUT'])
