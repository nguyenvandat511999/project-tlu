import traceback
import datetime
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.common.message import ErrorMessage
from application.models.document_expires import DocumentExpires
from application.controllers.api.document_expires import convert_day_to_timestamp
from application.controllers.cores.http_client import format_data_return

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def update_document_expires(request, token: Token):
    object = 'document'
    method = request.method
    try:
        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                records_document = db.session.query(Document).filter(
                    Document.deleted == False).all()

                for data_record in records_document:
                    record_expires = db.session.query(DocumentExpires).filter(
                        DocumentExpires.unit_id == data_record.unit_id).filter(
                        DocumentExpires.document_type_id == data_record.document_type_id).filter(
                        DocumentExpires.deleted == False).first()

                    if record_expires:
                        data_record.end_at = convert_day_to_timestamp(record_expires.day)

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': []
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
