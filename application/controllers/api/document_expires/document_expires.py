import traceback
import datetime
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.const import DocumentStatus
from application.models.document import Document
from application.common.message import ErrorMessage
from application.models.document_expires import DocumentExpires
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_expires(request, token: Token):
    object = 'document'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                time_now = datetime.datetime.now()
                time_expires = int(time_now.timestamp())

                records_document = db.session.query(Document).filter(
                    Document.end_at >= time_expires).filter(
                    Document.status != DocumentStatus.FINISH).filter(
                    Document.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_document in records_document:
                    list_data_return.append(to_dict(data_record_document))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
