from re import U
import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.common.message import ErrorMessage
from application.database import db
from application.models.unit import Unit
from application.models.document import Document
from application.models.document_type import DocumentType
from application.models.document_expires import DocumentExpires
from application.controllers.api.document_expires import convert_day_to_timestamp
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def expires_id(request, token: Token, id):
    object = 'expires_id'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_expires = db.session.query(DocumentExpires).filter(
                    DocumentExpires.id == id).filter(
                    DocumentExpires.deleted == False).first()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_expires),
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            day = data.get('day')
            unit_id = data.get('unit_id')
            document_type_id = data.get('document_type_id')

            if not day:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": day trống",
                                          response_http_code=400)

            if string_is_not_valid_uuid(unit_id) or string_is_not_valid_uuid(document_type_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": unit_id, document_type_id trống",
                                          response_http_code=400)

            record_expires = db.session.query(DocumentExpires).filter(
                DocumentExpires.unit_id == unit_id).filter(
                DocumentExpires.document_type_id == document_type_id).filter(
                DocumentExpires.deleted == False).first()

            if record_expires and record_expires.id != id:
                return format_data_return(response_message=ErrorMessage.DATA_IS_EXISTS + ": đã tồn tại",
                                          response_http_code=405)

            try:
                record_expires = db.session.query(DocumentExpires).filter(
                    DocumentExpires.id == id).filter(
                    DocumentExpires.deleted == False).first()

                if str(record_expires.unit_id) != unit_id:
                    record_unit = db.session.query(Unit).filter(
                        Unit.id == unit_id).filter(
                        Unit.deleted == False).first()

                    if not record_unit:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": cơ sở không tồn tại",
                                                  response_http_code=404)

                    record_expires.unit_id = record_unit.id
                    record_expires.unit_name = record_unit.name

                    record_unit.document_type.append(record_expires)

                if str(record_expires.document_type_id) != document_type_id:
                    record_document_type = db.session.query(DocumentType).filter(
                        DocumentType.id == document_type_id).filter(
                        DocumentType.deleted == False).first()

                    if not record_document_type:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": kiểu văn bản không tồn tại",
                                                  response_http_code=404)

                    record_expires.document_type_id = record_document_type.id
                    record_expires.document_type_name = record_document_type.name

                    record_document_type.unit.append(record_expires)

                record_expires.day = day

                records_document = db.session.query(Document).filter(
                    Document.unit_id == unit_id).filter(
                    Document.document_type_id == document_type_id).filter(
                    Document.deleted == False).all()

                if records_document:
                    for data_record in records_document:
                        data_record.end_at = convert_day_to_timestamp(day)

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_expires)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_expires = db.session.query(DocumentExpires).filter(
                    DocumentExpires.id == id).filter(
                    DocumentExpires.deleted == False).first()

                record_expires.deleted = True

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': {}
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
