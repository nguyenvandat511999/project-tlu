import time


def convert_day_to_timestamp(day):
    day_return = day*86400 + time.time()
    return day_return
