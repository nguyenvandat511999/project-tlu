import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.unit import Unit
from application.models.document import Document
from application.common.message import ErrorMessage
from application.models.document_type import DocumentType
from application.models.document_expires import DocumentExpires
from application.controllers.api.document import convert_day_to_timestamp_update
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def document_id(request, token: Token, id):
    object = 'document'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_document)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            code = data.get('code')
            name = data.get('name')
            type = data.get('type')
            draft = data.get('draft')
            signer = data.get('signer')
            sign_day = data.get('sign_day')
            file_name = data.get('file_name')
            description = data.get('description')
            unit_id = data.get('unit_id')
            document_type_id = data.get('document_type_id')
            created_at = data.get('created_at')

            if not draft:
                if string_is_null_or_empty(name) or string_is_null_or_empty(code) or string_is_null_or_empty(signer) \
                        or string_is_null_or_empty(description) or string_is_null_or_empty(file_name) or string_is_null_or_empty(type):
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name, code, signer, type, description, file_name trống",
                                              response_http_code=400)

                if not sign_day:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": sign_day trống",
                                              response_http_code=400)

                if string_is_not_valid_uuid(document_type_id) or string_is_not_valid_uuid(unit_id):
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_type_id, unit_id không đúng định dạng uuid",
                                              response_http_code=400)

                record_document = db.session.query(Document).filter(
                    Document.code == code).filter(
                    Document.deleted == False).first()

                if record_document and record_document.id != id:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": code đã tồn tại",
                                              response_http_code=400)

                record_document = db.session.query(Document).filter(
                    Document.name == name).filter(
                    Document.deleted == False).first()

                if record_document and record_document.id != id:
                    return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                              response_http_code=400)

            try:
                if document_type_id:
                    record_document_type = db.session.query(DocumentType).filter(
                        DocumentType.id == document_type_id).filter(
                        DocumentType.deleted == False).first()

                    if not record_document_type:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document_type không tồn tại",
                                                  response_http_code=404)

                if unit_id:
                    record_unit = db.session.query(Unit).filter(
                        Unit.id == unit_id).filter(
                        Unit.deleted == False).first()

                    if not record_unit:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": unit không tồn tại",
                                                  response_http_code=404)

                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": document không tồn tại",
                                              response_http_code=404)

                record_expires = db.session.query(DocumentExpires).filter(
                    DocumentExpires.unit_id == record_unit.id).filter(
                    DocumentExpires.document_type_id == record_document_type.id).filter(
                    DocumentExpires.deleted == False).first()

                day_expires = record_expires.day if record_expires is not None else None

                record_document.name = name
                record_document.code = code
                record_document.type = type
                record_document.signer = signer
                record_document.sign_day = sign_day
                record_document.file_name = file_name
                record_document.description = description
                record_document.end_at = convert_day_to_timestamp_update(day_expires, record_document.created_at) if day_expires is not None else None
                record_document.document_type_id = record_document_type.id if record_document_type else None
                record_document.document_type_name = record_document_type.name if record_document_type else None
                record_document.unit_id = record_unit.id if record_unit else None
                record_document.unit_name = record_unit.name if record_unit else None
                record_document.created_at = created_at

                if draft:
                    record_document.status = 'draft'

                db.session.commit()

                return format_data_return(response_data={'data': {
                    'result': to_dict(record_document)
                }},
                    response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                record_document = db.session.query(Document).filter(
                    Document.id == id).filter(
                    Document.deleted == False).first()

                if not record_document:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_document.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': {}
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
