from application.server import app
from application.controllers.api.document.document import document
from application.controllers.api.document.document_id import document_id

app.add_route(document_id, 'document/api/v1/web/document-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(document, 'document/api/v1/web/document-management/',
              methods=['POST', 'GET'])
