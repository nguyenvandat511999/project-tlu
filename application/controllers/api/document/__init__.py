
import time

from gatco_restapi.helpers import to_dict
from application.const import DocumentStatus


def convert_day_to_timestamp(day):
    day_return = day*86400 + time.time()
    return day_return


def convert_day_to_timestamp_update(day, created_at):
    day_return = day*86400 + created_at
    return day_return


def convert_data_document_return(records_document):
    data_return = {}

    document_init = []
    document_process = []
    document_pending = []
    document_receive = []
    document_working = []
    document_finish = []
    num = 0
    for data_document in records_document:
        if data_document.status == DocumentStatus.INITIALIZATION:
            document_init.append(to_dict(data_document))
        elif data_document.status == DocumentStatus.PROCESS:
            document_process.append(to_dict(data_document))
        elif data_document.status == DocumentStatus.PENDING:
            document_pending.append(to_dict(data_document))
        elif data_document.status == DocumentStatus.RECEIVE:
            document_receive.append(to_dict(data_document))
        elif data_document.status == DocumentStatus.WORKING:
            document_working.append(to_dict(data_document))
        elif data_document.status == DocumentStatus.FINISH:
            document_finish.append(to_dict(data_document))
        num += 1

    list_data_document = list(document_init + document_process
                              + document_pending + document_receive
                              + document_working + document_finish)

    data_return['result'] = list_data_document
    data_return['total'] = num

    return data_return
