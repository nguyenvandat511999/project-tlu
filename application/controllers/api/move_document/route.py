from application.server import app
from application.controllers.api.move_document.move_document import move_document


app.add_route(move_document, 'document/api/v1/web/move-document/', methods=['POST'])
