import traceback
from sqlalchemy import *
from gatco_restapi.helpers import to_dict

from application.database import db
from application.models.document import Document
from application.models.department import Department
from application.models.department_document import DepartmentDocument

from application.const import DocumentStatus
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    list_string_is_not_valid_uuid,
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


# @jwt_required
async def move_document(request):
    object = 'move_document'
    method = request.method
    try:
        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            document_id = data.get('document_id')

            if list_string_is_not_valid_uuid(document_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": document_id không đúng định dạng uuid",
                                          response_http_code=400)

            try:
                record_department = db.session.query(Department).filter(
                    Department.parent_id == None).filter(
                    Department.deleted == False).first()

                if not record_department:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": department không tồn tại",
                                              response_http_code=404)

                list_id_document = []
                for data_id in document_id:
                    record_document = db.session.query(Document).filter(
                        Document.id == data_id).filter(
                        Document.deleted == False).first()

                    if record_document:
                        record_department_document = db.session.query(DepartmentDocument).filter(
                            DepartmentDocument.document_id == data_id).filter(
                            DepartmentDocument.deleted == False).first()

                        if not record_department_document:
                            list_id_document.append(data_id)

                records_document = db.session.query(Document).filter(
                    Document.id.in_(list_id_document)).filter(
                    Document.deleted == False).all()

                list_department_document = []
                for data_record_document in records_document:
                    record_department_document = DepartmentDocument()

                    record_department_document.document_id = data_record_document.id
                    record_department_document.department_id = record_department.id
                    record_department_document.status = DocumentStatus.PENDING

                    record_department.document.append(record_department_document)
                    data_record_document.department.append(record_department_document)
                    data_record_document.status = DocumentStatus.PENDING

                    list_department_document.append(record_department_document)

                db.session.add_all(list_department_document)
                db.session.commit()

                list_data_return = []
                for data_return in list_department_document:
                    list_data_return.append(to_dict(data_return))

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(list_data_return)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
