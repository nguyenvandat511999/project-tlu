from application.server import app
from application.controllers.api.position.position import position
from application.controllers.api.position.position_id import position_id

app.add_route(position_id, 'position/api/v1/web/position-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(position, 'position/api/v1/web/position-management/',
              methods=['POST', 'GET'])
