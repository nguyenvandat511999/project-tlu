import traceback
from gatco_restapi.helpers import to_dict
from sqlalchemy import *

from application.database import db
from application.models.position import Position
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    validate_user_login,
    string_is_not_valid_uuid
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def position_id(request, id, token: Token):
    object = 'position'
    method = request.method
    try:
        if method == 'GET':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                if id:
                    record_position = db.session.query(Position).filter(
                        Position.id == id).filter(
                        Position.deleted == False).first()

                    if not record_position:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                                  response_http_code=404)

                    if record_position.deleted:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + " position đã bị xóa",
                                                  response_http_code=404)

                    return format_data_return(response_data={'data': {
                                                                'result': to_dict(record_position)
                                                            }},
                                              response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'PUT':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            data = request.json
            name = data.get('name')
            description = data.get('description')
            position_id = data.get('parent_id')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            if string_is_not_valid_uuid(position_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": parent_id sai",
                                          response_http_code=400)

            record_position = db.session.query(Position).filter(
                Position.name == name).filter(
                Position.deleted == False).first()

            if record_position and record_position.id != id:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            try:
                record_position = db.session.query(Position).filter(
                    Position.id == id).filter(
                    Position.deleted == False).first()

                record_position.name = name
                record_position.description = description

                if str(record_position.parent_id) != position_id:
                    record_parent_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    if not record_parent_position:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy position",
                                                  response_http_code=404)

                    record_position.parent_id = record_parent_position.id
                    record_position.parent_name = record_parent_position.name

                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_position)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'DELETE':
            if not validate_user_login(method, token, object):
                return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
                                          response_http_code=401)

            try:
                record_position = db.session.query(Position).filter(
                    Position.id == id).filter(
                    Position.deleted == False).first()

                if not record_position:
                    return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy id",
                                              response_http_code=404)

                record_position.deleted = True
                db.session.commit()

                return format_data_return(response_data={'data': {}},
                                          response_http_code=200)
            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
