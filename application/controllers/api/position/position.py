import traceback
from gatco_restapi.helpers import to_dict
from sqlalchemy import *

from application.database import db
from application.models.position import Position
from application.common.message import ErrorMessage
from application.controllers.cores.http_client import format_data_return
from application.controllers.cores.utilities.check_data import (
    string_is_null_or_empty,
    string_is_not_valid_uuid,
    validate_user_login
)

from sanic_jwt_extended.tokens import Token
from sanic_jwt_extended import (
    jwt_required
)


@jwt_required
async def position(request, token: Token):
    object = 'position'
    method = request.method
    try:
        if method == 'GET':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            try:
                records_position = db.session.query(Position).filter(
                    Position.deleted == False).all()

                list_data_return = []
                num_result = 0
                for data_record_position in records_position:
                    list_data_return.append(to_dict(data_record_position))
                    num_result += 1

                return format_data_return(response_data={'data': {
                                                            'result': list_data_return,
                                                            'total': num_result
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

        if method == 'POST':
            # if not validate_user_login(method, token, object):
            #     return format_data_return(response_message=ErrorMessage.UNAUTHORIZED_USER,
            #                               response_http_code=401)

            data = request.json
            name = data.get('name')
            description = data.get('description')
            position_id = data.get('parent_id')

            if string_is_null_or_empty(name):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name trống",
                                          response_http_code=400)

            if position_id and string_is_not_valid_uuid(position_id):
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": parent_id sai",
                                          response_http_code=400)

            record_position = db.session.query(Position).filter(
                Position.name == name).filter(
                Position.deleted == False).first()

            if record_position:
                return format_data_return(response_message=ErrorMessage.PARAM_ERROR + ": name đã tồn tại",
                                          response_http_code=400)

            try:
                record_position = Position()

                record_position.name = name
                record_position.description = description

                if position_id:
                    record_parent_position = db.session.query(Position).filter(
                        Position.id == position_id).filter(
                        Position.deleted == False).first()

                    if not record_parent_position:
                        return format_data_return(response_message=ErrorMessage.DATA_NOT_FOUND + ": không tìm thấy position",
                                                  response_http_code=404)

                    record_position.parent_id = record_parent_position.id
                    record_position.parent_name = record_parent_position.name

                db.session.add(record_position)
                db.session.commit()

                return format_data_return(response_data={'data': {
                                                            'result': to_dict(record_position)
                                                        }},
                                          response_http_code=200)

            except Exception as e:
                print(traceback.format_exc())
                return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                          response_http_code=400)

    except Exception as e:
        print(traceback.format_exc())
        return format_data_return(response_message=ErrorMessage.SYSTEM_ERROR + str(e),
                                  response_http_code=400)

    finally:
        if db.session:
            db.session.close()
