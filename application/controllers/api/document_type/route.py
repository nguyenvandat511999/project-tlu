from application.server import app
from application.controllers.api.document_type.document_type import document_type
from application.controllers.api.document_type.document_type_id import document_type_id

app.add_route(document_type_id, 'document-type/api/v1/web/document-type-management/<id:uuid>',
              methods=['GET', 'PUT', 'DELETE'])
app.add_route(document_type, 'document-type/api/v1/web/document-type-management/',
              methods=['POST', 'GET'])
