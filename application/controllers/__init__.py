from application.extensions import jinja


def init_controllers(app):
    import application.controllers.api.account.route
    import application.controllers.api.staff.route
    import application.controllers.api.department.route
    import application.controllers.api.position.route
    import application.controllers.api.user_role.route
    import application.controllers.api.permission.route
    import application.controllers.api.document.route
    import application.controllers.api.document_type.route
    import application.controllers.api.upload.route
    import application.controllers.api.unit.route
    import application.controllers.api.move_document.route
    import application.controllers.api.document_admin.route
    import application.controllers.api.document_manager.route
    import application.controllers.api.document_staff.route
    import application.controllers.api.document_report.route
    import application.controllers.api.statistic.route
    import application.controllers.api.document_expires.route

    @app.route('/')
    def index(request):
        return jinja.render('index.html', request)
