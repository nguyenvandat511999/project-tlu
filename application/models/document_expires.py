from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import UUID

from application.database import db
from application.database.model import CommonModel


class DocumentExpires(CommonModel):
    __tablename__ = "document_expires"
    day = db.Column(Integer())
    description = db.Column(String(500))
    unit = relationship("Unit", backref=backref("document_expires", cascade="all"))
    unit_id = db.Column(UUID(as_uuid=True), ForeignKey('unit.id'))
    unit_name = db.Column(String(250))
    document_type = relationship("DocumentType", backref=backref("document_expires", cascade="all"))
    document_type_id = db.Column(UUID(as_uuid=True), ForeignKey('document_type.id'))
    document_type_name = db.Column(String(250))
