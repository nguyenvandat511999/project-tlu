from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class Document(CommonModel):
    __tablename__ = "document"
    code = db.Column(String(250))
    name = db.Column(String(500), nullable=False, index=True)
    description = db.Column(String(500))
    file_name = db.Column(String(500))
    sign_day = db.Column(BigInteger())
    signer = db.Column(String(100))
    status = db.Column(String(100))
    type = db.Column(String(250))
    end_at = db.Column(BigInteger())
    unit = relationship("Unit", back_populates='document')
    unit_id = db.Column(UUID(as_uuid=True), ForeignKey('unit.id'))
    unit_name = db.Column(String(250))
    document_type = relationship("DocumentType", back_populates='document')
    document_type_id = db.Column(UUID(as_uuid=True), ForeignKey('document_type.id'))
    document_type_name = db.Column(String(250))
    department = relationship('Department', secondary='department_document', viewonly=True)
    staff = relationship('Staff', secondary='staff_document', viewonly=True)
    work_report = relationship("WorkReport", back_populates='document')
