from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class Department(CommonModel):
    __tablename__ = "department"
    name = db.Column(String(50), nullable=False, index=True)
    description = db.Column(String(250))
    created_at = db.Column(BigInteger())
    staff = relationship("Staff", back_populates='department')
    parent_id = db.Column(UUID(as_uuid=True), ForeignKey('department.id'))
    parent_name = db.Column(String(250))
    children = relationship('Department')
    document = relationship('Document', secondary='department_document', viewonly=True)
    work_report = relationship("WorkReport", back_populates='department')
