from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class Position(CommonModel):
    __tablename__ = "position"
    name = db.Column(String(50), nullable=False, index=True)
    description = db.Column(String(250))
    staff = relationship("Staff", back_populates='position')
    parent_id = db.Column(UUID(as_uuid=True), ForeignKey('position.id'))
    parent_name = db.Column(String(250))
    children = relationship('Position')
