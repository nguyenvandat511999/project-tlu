# from app.models.partner import *
from application.models.account import *
from application.models.staff import *
from application.models.department import *
from application.models.position import *
from application.models.user_role import *
from application.models.user_role_permission import *
from application.models.permission import *
from application.models.document import *
from application.models.document_type import *
from application.models.unit import *
from application.models.department_document import *
from application.models.staff_document import *
from application.models.work_report import *
from application.models.document_expires import *
