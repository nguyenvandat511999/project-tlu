from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class UserRolePermission(CommonModel):
    __tablename__ = "user_role_permission"
    user_role_id = db.Column(UUID(as_uuid=True), ForeignKey('user_role.id'))
    permission_id = db.Column(UUID(as_uuid=True), ForeignKey('permission.id'))
    user_role = relationship("UserRole", backref=backref("user_role_permission", cascade="all"))
    permission = relationship("Permission", backref=backref("user_role_permission", cascade="all"))
