from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class WorkReport(CommonModel):
    __tablename__ = 'work_report'
    name = db.Column(String(255), nullable=False)
    description = db.Column(String(500), nullable=False)
    status = db.Column(String(500), nullable=False)
    review_description = db.Column(String(500))
    document = relationship("Document", back_populates="work_report")
    document_id = db.Column(UUID(as_uuid=True), ForeignKey('document.id'))
    document_name = db.Column(String(250))
    staff = relationship("Staff", back_populates="work_report")
    staff_id = db.Column(UUID(as_uuid=True), ForeignKey('staff.id'))
    staff_name = db.Column(String(250))
    staff_code = db.Column(String(250))
    department = relationship("Department", back_populates="work_report")
    department_id = db.Column(UUID(as_uuid=True), ForeignKey('department.id'))
    department_name = db.Column(String(250))
