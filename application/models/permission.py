from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class Permission(CommonModel):
    __tablename__ = "permission"
    name = db.Column(String(50), nullable=False, index=True)
    method = db.Column(String(250))
    type = db.Column(String(250))
    user_role = relationship('UserRole', secondary='user_role_permission', viewonly=True)
