from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.models.staff import Staff
from application.database.model import CommonModel


class Account(CommonModel):
    __tablename__ = 'account'
    username = db.Column(String(255), nullable=False, index=True, unique=True)
    password = db.Column(String(), nullable=False, index=True)
    active = db.Column(Boolean(), default=False)
    user_role = relationship("UserRole", back_populates="account")
    role_id = db.Column(UUID(as_uuid=True), ForeignKey('user_role.id'))
    role_name = db.Column(String(250))
    staff = relationship("Staff", back_populates="account")
    staff_id = db.Column(UUID(as_uuid=True), ForeignKey('staff.id'))
    staff_name = db.Column(String(250))
    staff_code = db.Column(String(250))
