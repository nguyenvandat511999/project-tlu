from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class UserRole(CommonModel):
    __tablename__ = "user_role"
    name = db.Column(String(50), nullable=False, index=True)
    description = db.Column(String(250))
    account = relationship("Account", back_populates="user_role")
    permission = relationship('Permission', secondary='user_role_permission', viewonly=True)
