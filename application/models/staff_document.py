from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class StaffDocument(CommonModel):
    __tablename__ = "staff_document"
    status = db.Column(String(250))
    description = db.Column(String(500))
    staff = relationship("Staff", backref=backref("staff_document", cascade="all"))
    staff_id = db.Column(UUID(as_uuid=True), ForeignKey('staff.id'))
    staff_name = db.Column(String(250))
    document = relationship("Document", backref=backref("staff_document", cascade="all"))
    document_id = db.Column(UUID(as_uuid=True), ForeignKey('document.id'))
    document_name = db.Column(String(250))
    document_type_id = db.Column(UUID(as_uuid=True))
