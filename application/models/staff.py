from sqlalchemy import *
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.orm import relationship

from application.database import db
from application.database.model import CommonModel


class Staff(CommonModel):
    __tablename__ = "staff"
    name = db.Column(String(250), nullable=False)
    code = db.Column(String(50), nullable=False, index=True)
    email = db.Column(String(250))
    address = db.Column(String(250))
    phone_number = db.Column(String(50))
    gender = db.Column(Integer())
    birthday = db.Column(BigInteger())
    status = db.Column(String(50))
    created_at = db.Column(BigInteger())
    account = relationship("Account", back_populates='staff')
    position = relationship("Position", back_populates='staff')
    position_id = db.Column(UUID(as_uuid=True), ForeignKey('position.id'))
    position_name = db.Column(String(250))
    department = relationship("Department", back_populates='staff')
    department_id = db.Column(UUID(as_uuid=True), ForeignKey('department.id'))
    department_name = db.Column(String(250))
    document = relationship('Document', secondary='staff_document', viewonly=True)
    work_report = relationship("WorkReport", back_populates='staff')
