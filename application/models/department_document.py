from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class DepartmentDocument(CommonModel):
    __tablename__ = "department_document"
    status = db.Column(String(250))
    description = db.Column(String(500))
    document = relationship("Document", backref=backref("department_document", cascade="all"))
    document_id = db.Column(UUID(as_uuid=True), ForeignKey('document.id'))
    document_name = db.Column(String(250))
    document_type_id = db.Column(UUID(as_uuid=True))
    department_name = db.Column(String(250))
    department_id = db.Column(UUID(as_uuid=True), ForeignKey('department.id'))
    department = relationship("Department", backref=backref("department_document", cascade="all"))
