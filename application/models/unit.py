from sqlalchemy import *
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB, UUID

from application.database import db
from application.database.model import CommonModel


class Unit(CommonModel):
    __tablename__ = "unit"
    name = db.Column(String(50), nullable=False, index=True)
    description = db.Column(String(250))
    document = relationship("Document", back_populates='unit')
    document_type = relationship('DocumentType', secondary='document_expires', viewonly=True)
