from pymongo.common import UNAUTHORIZED_CODES


class ErrorMessage(object):
    SYSTEM_ERROR = "Hệ thống xảy ra lỗi, vui lòng thử lại sau "
    PARAM_ERROR = "Dữ liệu đầu vào chưa chính xác "
    DATA_NOT_FOUND = "Không tìm thấy dữ liệu hợp lệ "
    DATA_IS_EXISTS = "Dữ liệu đã tồn tại "
    UNAUTHORIZED_USER = "Không được cấp quyền"
