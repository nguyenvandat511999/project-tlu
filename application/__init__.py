import os

from sanic_cors import CORS

from application.config import ConfigLocal
from application.controllers import init_controllers
from application.database import init_database
from application.extensions import init_extensions
from application.models import *
from .server import app


def run_app(host="127.0.0.1", port=9000, num_workers=os.cpu_count(), debug=False):
    if os.environ.get("ENVIRONMENT") == "LOCAL":
        app.config.from_object(ConfigLocal)

    init_database(app)
    init_extensions(app)
    init_controllers(app)
    CORS(app, automatic_options=True)

    """ Function for bootstrapping gatco app. """
    app.run(host=host, port=port, debug=debug, workers=int(num_workers), access_log=True)
